﻿namespace EatUpp.Mobile.Common.Enums
{
    public enum SROn
    {
        None,
        Connected,
        Disconnected,
        MessageReceived,
        BookReceived,
        CustomerPoked,
        FoodOrderReceived,
        ApprovementReceived,
        RejectionReceived,
        NotifyRestaurantOnlineStatus
    }
}
