﻿namespace EatUpp.Mobile.Common.Enums
{
    public enum HubFunctions
    {
        None,
        SendMessage,
        PlaceBook,
        PokeRestaurant,
        OrderFood,
        ApproveBook,
        RejectBook
    }
}