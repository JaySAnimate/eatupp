﻿namespace EatUpp.Mobile.Common.Enums
{
    public enum Role
    {
        None,
        Restaurant,
        Customer
    }
}