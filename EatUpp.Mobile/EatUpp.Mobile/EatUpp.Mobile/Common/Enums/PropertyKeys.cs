﻿namespace EatUpp.Mobile.Common.Enums
{
    public enum PropertyKeys
    {
        Token,
        RestaurantId,
        TableId,
        CustomerEmailSRID,
    }
}