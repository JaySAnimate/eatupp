﻿namespace EatUpp.Mobile.Common.Enums
{
    public enum ErrorType
    {
        UserNotFound,
        Crash,
        OrderNotSaved,
    }
}