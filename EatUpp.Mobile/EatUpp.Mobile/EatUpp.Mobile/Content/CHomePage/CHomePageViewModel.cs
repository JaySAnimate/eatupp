﻿using System;
using Xamarin.Forms;
using EatUpp.Mobile.Misc;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using AsyncAwaitBestPractices.MVVM;
using Microsoft.AspNetCore.SignalR.Client;

namespace EatUpp.Mobile.Content
{
    public class CHomePageViewModel : BaseViewModel
    {
        #region Properties

        private bool _isScanButtonVisible = true;
        public bool IsScanButtonVisible
        {
            get => _isScanButtonVisible; 
            set => SetValue(ref _isScanButtonVisible, value);
        }

        #endregion

        #region Commands

        public IAsyncCommand ScanCommand => new AsyncCommand(Scan);

        #endregion

        #region Methods

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously. //EATUPP(It Sould be like that to make UI thread independent)
        private async Task Scan()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously. //EATUPP(It Sould be like that to make UI thread independent)
        {
            try
            {
                IsScanButtonVisible = false;

                if (SignalR.HubConnection is null || SignalR.HubConnection.State == HubConnectionState.Disconnected)
                    SignalR.ConnectSignalR().SafeFireAndForget(ex => Helpers.LogError(ex));

                Device.BeginInvokeOnMainThread(() => Navigation.PushAsync(new QRScannerPage()));

                IsScanButtonVisible = true;
            }
            catch (OperationCanceledException ex)
            {
                Helpers.LogError(ex);
                IsScanButtonVisible = true;
                Shell.Current.GoToAsync("homepage").SafeFireAndForget(ex => Helpers.LogError(ex));
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Operation is canceled", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
            }
            catch (Exception ex)
            {
                Helpers.LogError(ex);
                IsScanButtonVisible = true;
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert(ex.Message, "Try again", "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
        }

        #endregion
    }
}