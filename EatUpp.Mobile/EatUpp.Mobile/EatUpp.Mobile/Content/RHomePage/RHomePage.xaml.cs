﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RHomePage : ContentPage
    {
        public RHomePage()
        {
            InitializeComponent();
        }
    }
}