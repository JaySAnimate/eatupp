﻿using Xamarin.Forms;
using System.Windows.Input;
using EatUpp.Mobile.Models.Data;
using System.Collections.ObjectModel;
using AsyncAwaitBestPractices;
using EatUpp.Mobile.Misc;
using EatUpp.Mobile.Models.Request;
using EatUpp.Mobile.Common.Enums;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System;
using EatUpp.Mobile.Models.Response;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace EatUpp.Mobile.Content
{
    public class RHomePageViewModel : BaseViewModel
    {
        #region Private Fields

        private const string _endpoint = "/api/Restaurant/GetOrders";

        #endregion

        #region Properties

        private ObservableCollection<OrderCard> _orders;
        public ObservableCollection<OrderCard> Orders
        {
            get => _orders;
            set => SetValue(ref _orders, value);
        }

        private OrderCard _selectedOrder;
        public OrderCard SelectedOrder
        {
            get => _selectedOrder;
            set => SetValue(ref _selectedOrder, value);
        }

        #endregion

        public RHomePageViewModel()
        {
            SignalR.OnOrderReceived += PushOrder;
            GetOrdersFor24(new GetOrderRequest
            {
                RestaurantId = int.Parse(EatUppApp.Properties[PropertyKeys.RestaurantId.ToString()].ToString()),
                From = DateTime.Now - TimeSpan.FromHours(24)
            }).SafeFireAndForget(ex => Helpers.LogError(ex));
        }

        #region Commands

        public ICommand ShowOrderCommand => new Command(ShowOrder);

        #endregion

        #region Methods

        private async Task GetOrdersFor24(GetOrderRequest request)
        {
            var cToken = EatUppApp.CToken;
            try
            {
                var postContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
                var responseMessage = await HttpClient.PostAsync($"{_url}{_endpoint}", postContent, cToken.Token);
                cToken.Dispose();

                if (!responseMessage.IsSuccessStatusCode)
                    throw new Exception(responseMessage.ReasonPhrase);

                var jsonResponse = await responseMessage.Content?.ReadAsStringAsync();
                var response = JsonConvert.DeserializeObject<GetDataResponse<IEnumerable<FoodOrderData>>>(jsonResponse);
                if (response.Error)
                    throw new Exception(response.ErrorResponse.ErrorDescription);

                var orders = new ObservableCollection<OrderCard>();
                foreach (var item in response.Data)
                {

                    orders.Add(new OrderCard
                    {
                        OrderTime = item.OrderTime.Value,
                        TableId = item.TableId,
                        Order = JsonConvert.DeserializeObject<ObservableCollection<Models.Data.MenuItem>>(item.Order)
                    });
                }

                Orders = orders;
            }
            catch (Exception ex)
            {
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Crash", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
            finally
            {
                cToken.Dispose();
            }
        }

        private void PushOrder(OrderInfo orderInfo)
        {
            if (Orders is null)
                Orders = new ObservableCollection<OrderCard>();

            Orders.Add(new OrderCard(orderInfo));
        }

        private void ShowOrder() => Device.BeginInvokeOnMainThread(() => Navigation.PushAsync(new OrderInfoPage(SelectedOrder.Order)).SafeFireAndForget(ex => Helpers.LogError(ex)));

        #endregion
    }
}