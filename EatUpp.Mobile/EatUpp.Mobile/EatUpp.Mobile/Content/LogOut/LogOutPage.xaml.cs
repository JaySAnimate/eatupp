﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using EatUpp.Mobile.Misc;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using Microsoft.AspNetCore.SignalR.Client;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LogOutPage : ContentPage
    {
        public LogOutPage()
        {
            InitializeComponent();
            LogOut().SafeFireAndForget(ex => Helpers.LogError(ex));
        }
        public async Task LogOut()
        {
            try
            {
                Application.Current.Properties.Clear();
                if (SignalR.HubConnection.State == HubConnectionState.Connected)
                    await SignalR.HubConnection.StopAsync();
                Application.Current.MainPage = new NavigationPage(new FastSign());
            }
            catch (Exception ex)
            {
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    Application.Current.MainPage.DisplayAlert("Crash", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
        }
    }
}