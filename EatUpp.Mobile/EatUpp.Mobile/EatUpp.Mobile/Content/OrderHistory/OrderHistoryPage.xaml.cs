﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderHistoryPage : ContentPage
    {
        public OrderHistoryPage()
        {
            InitializeComponent();
        }
    }
}