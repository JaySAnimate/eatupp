﻿using Xamarin.Forms;
using AsyncAwaitBestPractices.MVVM;

namespace EatUpp.Mobile.Content
{
    public class OrderHistoryViewModel : BaseViewModel
    {
        #region Commands

        public IAsyncCommand GoToBasketCommand => new AsyncCommand(async () => await Shell.Current.GoToAsync("basket",true));

        #endregion
    }
}