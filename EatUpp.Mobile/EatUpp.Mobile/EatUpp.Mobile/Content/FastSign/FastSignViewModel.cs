﻿using System;
using Xamarin.Forms;
using Newtonsoft.Json;
using EatUpp.Mobile.Misc;
using System.Windows.Input;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using EatUpp.Mobile.Common.Enums;
using AsyncAwaitBestPractices.MVVM;
using EatUpp.Mobile.Models.Response;

namespace EatUpp.Mobile.Content
{
    public class FastSignViewModel : BaseViewModel
    {
        #region Private Fields

        private const string _endpoint = "/api/User/FastSign?email=";

        #endregion

        #region Properties

        private string _email;
        public string Email
        {
            get => _email;
            set => SetValue(ref _email, value);
        }

        private bool _isEmailStackVisible = true;
        public bool IsEmailStackVisible
        {
            get => _isEmailStackVisible;
            set => SetValue(ref _isEmailStackVisible, value);
        }

        #endregion

        #region Commands

        public IAsyncCommand ConnectCommandAsync => new AsyncCommand(Connect);

        public ICommand GoRestaurantLoginPageCommand => new Command(() => Device.BeginInvokeOnMainThread(() => Navigation.PushAsync(new LoginPage())));

        #endregion

        #region Methods

        private async Task Connect()
        {
            try
            {
                IsEmailStackVisible = false;

                await FastSignCustomerAsync(Email);

                SignalR.ConnectSignalR().SafeFireAndForget(ex => Helpers.LogError(ex));

                EatUppApp.MainPage = new CEatUppShell();
            }
            catch (OperationCanceledException ex)
            {
                IsEmailStackVisible = true;
                Helpers.LogError(ex);
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Operation is canceled", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
            }
            catch (Exception ex)
            {
                IsEmailStackVisible = true;
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Connection failed", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
                return;
            }
        }

        public async Task FastSignCustomerAsync(string email)
        {
            var cToken = EatUppApp.CToken;
            try
            {
                var responseMessage = await HttpClient.GetAsync($"{_url}{_endpoint}{email}", cToken.Token);
                cToken.Dispose();

                if (!responseMessage.IsSuccessStatusCode)
                    throw new Exception(responseMessage.ReasonPhrase);

                var signUpResponse = JsonConvert.DeserializeObject<SignUpResponse>(await responseMessage.Content?.ReadAsStringAsync());
                if (signUpResponse.Error)
                    throw new Exception(signUpResponse.ErrorResponse.ErrorDescription);

                EatUppApp.Properties[PropertyKeys.CustomerEmailSRID.ToString()] = signUpResponse.Email;
                EatUppApp.Properties[PropertyKeys.Token.ToString()] = signUpResponse.Token;
                EatUppApp.SavePropertiesAsync().SafeFireAndForget(ex => Helpers.LogError(ex));
            }
            catch
            {
                throw;
            }
            finally
            {
                cToken.Dispose();
            }
        }

        #endregion
    }
}