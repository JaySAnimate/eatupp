﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FastSign : ContentPage
    {
        public FastSign()
        {
            InitializeComponent();
        }

        private void GoRestaurantGesture_Tapped(object sender, EventArgs e) => PartnerLabel.TextColor = Color.Red;

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            EmailStack.IsVisible = true;
            PartnerLabel.TextColor = Color.White;
        }
    }
}