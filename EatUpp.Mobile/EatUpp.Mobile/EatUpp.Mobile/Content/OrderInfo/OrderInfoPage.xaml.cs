﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using EatUpp.Mobile.Models.Data;
using System.Collections.ObjectModel;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderInfoPage : ContentPage
    {
        public OrderInfoPage(ObservableCollection<Models.Data.MenuItem> order)
        {
            InitializeComponent();
            BindingContext = new OrderInfoViewModel { Orders = order };
        }
    }
}