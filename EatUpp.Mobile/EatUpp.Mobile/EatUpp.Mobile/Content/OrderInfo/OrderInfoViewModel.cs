﻿using EatUpp.Mobile.Models.Data;
using System.Collections.ObjectModel;

namespace EatUpp.Mobile.Content
{
    class OrderInfoViewModel : BaseViewModel
    {
        #region Properties

        private ObservableCollection<MenuItem> _orders;
        public ObservableCollection<MenuItem> Orders
        {
            get => _orders; 
            set => SetValue(ref _orders, value);
        }

        #endregion
    }
}
