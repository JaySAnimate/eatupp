﻿using Xamarin.Forms;

namespace EatUpp.Mobile.Content
{
    public class RShellViewModel:BaseViewModel
    {
        public RShellViewModel()
        {
            Routing.RegisterRoute("homepage", typeof(CHomePage));
            Routing.RegisterRoute("orderhistory", typeof(OrderHistoryPage));
            Routing.RegisterRoute("aboutus", typeof(AboutUsPage));
            Routing.RegisterRoute("logout", typeof(LogOutPage));
        }
    }
}
