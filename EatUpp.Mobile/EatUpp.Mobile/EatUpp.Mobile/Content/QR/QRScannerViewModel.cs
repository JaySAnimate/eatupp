﻿using ZXing;
using System;
using Xamarin.Forms;
using Newtonsoft.Json;
using EatUpp.Mobile.Misc;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using EatUpp.Mobile.Models.Data;
using EatUpp.Mobile.Common.Enums;
using AsyncAwaitBestPractices.MVVM;
using Microsoft.AspNetCore.SignalR.Client;

namespace EatUpp.Mobile.Content
{
    public class QRScannerViewModel : BaseViewModel
    {
        public IAsyncCommand<Result> ScanResultCommand => new AsyncCommand<Result>(PokeRestaurant);

        #region Methods

        private async Task PokeRestaurant(Result result)
        {
            var cToken = EatUppApp.CToken;
            try
            {
                var postCard = JsonConvert.DeserializeObject<PostCard>(result.Text);
                
                while (SignalR.HubConnection.State != HubConnectionState.Connected) { }
                await SignalR.HubConnection.InvokeAsync(HubFunctions.PokeRestaurant.ToString(), postCard, cToken.Token);
                cToken.Dispose();

                EatUppApp.Properties[PropertyKeys.TableId.ToString()] = postCard.TableId;

                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PopAsync().SafeFireAndForget(ex => Helpers.LogError(ex));
                    Navigation.PushAsync(new MenuPage(int.Parse(postCard.RestaurantId))).SafeFireAndForget(ex => Helpers.LogError(ex));
                });
            }
            catch (OperationCanceledException ex)
            {
                Helpers.LogError(ex);
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Operation is canceled", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
            }
            catch (Exception ex)
            { 
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Crash", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
            finally
            {
                cToken.Dispose();
            }
        }

        #endregion
    }
}