﻿using ZXing;
using System;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QRScannerPage : ZXingScannerPage
    {
        public QRScannerPage()
        {
            InitializeComponent();
            ScannerView.OnScanResult += result => ScannerView.IsScanning = false;
            ScannerView.AutoFocus();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ScannerView.IsScanning = true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            ScannerView.IsScanning = false;
        }

        private void Scanner_Tapped(object sender, EventArgs e) => ScannerView.Focus();
    }
}