﻿using Xamarin.Forms;

namespace EatUpp.Mobile.Content
{
    public class CShellViewModel : BaseViewModel
    {
        public CShellViewModel()
        {
            Routing.RegisterRoute("basket", typeof(BasketPage));
            Routing.RegisterRoute("homepage", typeof(CHomePage));
            Routing.RegisterRoute("aboutus", typeof(AboutUsPage));
            Routing.RegisterRoute("restaurants", typeof(RestaurantsPage));
            Routing.RegisterRoute("orderhistory", typeof(OrderHistoryPage));
        }
    }
}