﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        public MenuPage(int restaurantId)
        {
            BindingContext = new MenuPageViewModel(restaurantId);
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var vm = BindingContext as MenuPageViewModel;
            vm.NotifyPropertyChanged(nameof(vm.CollectedOrderCount));
        }
    }
}