﻿using System;
using System.Linq;
using Xamarin.Forms;
using Newtonsoft.Json;
using EatUpp.Mobile.Misc;
using System.Windows.Input;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using EatUpp.Mobile.Models.Data;
using System.Collections.ObjectModel;
using MenuItem = EatUpp.Mobile.Models.Data.MenuItem;

namespace EatUpp.Mobile.Content
{
    public class MenuPageViewModel : BaseViewModel
    {
        #region Pivate Fields

        private const string _menuEndpoint = "/api/Customer/GetMenuOf?restaurantId=";
        private readonly int _restaurantId;

        #endregion

        #region Properties

        private ObservableCollection<MenuItem> _menu;
        public ObservableCollection<MenuItem> Menu
        {
            get => _menu;
            set => SetValue(ref _menu, value);
        }

        private ObservableCollection<FoodCategoryData> _categories;
        public ObservableCollection<FoodCategoryData> Categories
        {
            get => _categories;
            set => SetValue(ref _categories, value);
        }

        private FoodCategoryData _selectedCategory;
        public FoodCategoryData SelectedCategory
        {
            get => _selectedCategory;
            set => SetValue(ref _selectedCategory, value);
        }

        private MenuItem _selectedFood;
        public MenuItem SelectedFood
        {
            get => _selectedFood;
            set => SetValue(ref _selectedFood, value);
        }

        private bool _delivered;
        public bool Delivered
        {
            get => _delivered;
            set => SetValue(ref _delivered, value);
        }

        public string CollectedOrderCount => CollectedOrder.OrderItemsQuantity.ToString();

        #endregion

        public MenuPageViewModel(int restaurantId)
        {
            _restaurantId = restaurantId;
            FetchMenu();
        }

        #region Commands

        public ICommand AddToSelectionCommand => new Command<MenuItem>(AddToSelection);
        public ICommand GoToBasketCommand => new Command(() => Shell.Current.GoToAsync("basket", true).SafeFireAndForget(ex => Helpers.LogError(ex)));
        public ICommand BackButtonCommand => new Command(BackBuutonFunctionality);
        public ICommand FetchManuCommand => new Command(FetchMenu);

        #endregion

        #region Methods

        private void FetchMenu()
        {
            GetMenu(_restaurantId).ContinueWith(async result =>
            {
                var menu = await result;
                Categories = new ObservableCollection<FoodCategoryData>(menu.Select(menuItem => menuItem.FoodCategory).Distinct(new CategoryComparer()));
                Menu = menu;
                SelectedCategory = Categories.FirstOrDefault();
            }).SafeFireAndForget(ex => Helpers.LogError(ex));
        }

        private async Task<ObservableCollection<MenuItem>> GetMenu(int restaurantId)
        {
            var cToken = EatUppApp.CToken;
            try
            {
                var responseMessage = await HttpClient.GetAsync($"{_url}{_menuEndpoint}{restaurantId}", cToken.Token);
                cToken.Dispose();

                if (!responseMessage.IsSuccessStatusCode)
                    throw new Exception(responseMessage.ReasonPhrase);

                var jsonResponse = await responseMessage.Content?.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ObservableCollection<MenuItem>>(jsonResponse);
            }
            catch (OperationCanceledException ex)
            {
                Helpers.LogError(ex);
                Navigation.PopAsync().SafeFireAndForget(ex => Helpers.LogError(ex));
                return null;
            }
            catch (Exception ex)
            {
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Crash", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
                return null;
            }
            finally
            {
                cToken.Dispose();
            }
        }

        private void AddToSelection(MenuItem selectedFood)
        {
            var order = CollectedOrder.GetCollectedOrder();

            if (!order.Contains(selectedFood))
            {
                selectedFood.Count++;
                order.Add(selectedFood);
            }
            else
                order.Where(item => item.Id == selectedFood.Id).FirstOrDefault().Count++;

            NotifyPropertyChanged(nameof(CollectedOrderCount));
        }

        public void BackBuutonFunctionality()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var Yes = await EatUppApp.MainPage.DisplayAlert("Go to Homepage", "To display the menu again You'll need to rescan The QR code", "Yes", "No");
                if (Yes)
                    Navigation.PopToRootAsync().SafeFireAndForget(ex => Helpers.LogError(ex));
            });
        }

        #endregion
    }
}