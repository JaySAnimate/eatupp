﻿using System;
using System.Linq;
using Xamarin.Forms;
using Newtonsoft.Json;
using EatUpp.Mobile.Misc;
using System.Windows.Input;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using EatUpp.Mobile.Models.Data;
using EatUpp.Mobile.Common.Enums;
using AsyncAwaitBestPractices.MVVM;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.SignalR.Client;
using MenuItem = EatUpp.Mobile.Models.Data.MenuItem;

namespace EatUpp.Mobile.Content
{
    public class BasketPageViewModel : BaseViewModel
    {
        public ObservableCollection<MenuItem> CollectedOrders => CollectedOrder.GetCollectedOrder();

        #region Command

        public IAsyncCommand SendOrderCommand => new AsyncCommand(SendOrder);

        public ICommand PlusCommand => new Command<MenuItem>(menuItem => menuItem.Count++);

        public ICommand MinusCommand => new Command<MenuItem>(MinusMenuItem);

        public ICommand DeleteOrderCommand => new Command<MenuItem>(DeleteOrderMethod);

        #endregion

        #region Methods

        public void DeleteOrderMethod(MenuItem deleteitem) => CollectedOrders.Remove(deleteitem);

        public void MinusMenuItem(MenuItem countitem)
        {
            if (countitem.Count > 0)
                countitem.Count--;
        }

        private async Task SendOrder()
        {
            SendOrderCommand.RaiseCanExecuteChanged();
            var cToken = EatUppApp.CToken;
            try
            {
                if (!CollectedOrders.Any())
                    throw new Exception("Select at least one order.");

                var foodOrder = new FoodOrderData
                {
                    Order = JsonConvert.SerializeObject(CollectedOrders),
                    RestaurantId = CollectedOrders.First().RestaurantId,
                    TableId = (int)EatUppApp.Properties[PropertyKeys.TableId.ToString()],
                    OrderTime = DateTime.Now
                };
                var deliverd = await SignalR.HubConnection.InvokeAsync<bool>(HubFunctions.OrderFood.ToString(), foodOrder, cToken.Token);
                cToken.Dispose();

                if (!deliverd)
                    throw new Exception("Order is not Delivered\nEither restaurant is online or menu is nor registered.");

                CollectedOrders.Clear();
                SendOrderCommand.RaiseCanExecuteChanged();
            }
            catch (OperationCanceledException ex)
            {
                Helpers.LogError(ex);
                SendOrderCommand.RaiseCanExecuteChanged();
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Operation is canceled", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
            }
            catch (Exception ex)
            {
                Helpers.LogError(ex);
                SendOrderCommand.RaiseCanExecuteChanged();
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Crash", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
            finally
            {
                cToken.Dispose();
            }
        }

        #endregion
    }
}