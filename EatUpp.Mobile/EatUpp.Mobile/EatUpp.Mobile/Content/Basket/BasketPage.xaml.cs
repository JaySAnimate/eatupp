﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasketPage : ContentPage
    {
        public BasketPage()
        {
            InitializeComponent();

            var vm = new BasketPageViewModel();
            vm.SendOrderCommand.CanExecuteChanged += (sender, args) => OrderButton.IsEnabled = !OrderButton.IsEnabled;
            BindingContext = vm;
        }
    }
}