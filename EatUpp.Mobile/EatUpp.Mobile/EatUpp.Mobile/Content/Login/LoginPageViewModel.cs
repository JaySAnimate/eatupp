﻿using System;
using System.Text;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Net.Http;
using EatUpp.Mobile.Misc;
using System.Windows.Input;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using Xamarin.Forms.Internals;
using EatUpp.Mobile.Common.Enums;
using AsyncAwaitBestPractices.MVVM;
using EatUpp.Mobile.Models.Request;
using EatUpp.Mobile.Models.Response;

namespace EatUpp.Mobile.Content
{
    [Preserve(AllMembers = true)]
    public class LoginPageViewModel : BaseViewModel
    {
        #region Private Fields

        private const string _endpoint = "/api/User/LogIn";

        #endregion

        #region Properties

        private LogInUserModel _logInUserModel;
        public LogInUserModel LogInUserModel
        {
            get => _logInUserModel;
            set { SetValue(ref _logInUserModel, value); }
        }

        private bool _isDataStackVisible = true;
        public bool IsDataStackVisible
        {
            get => _isDataStackVisible;
            set => SetValue(ref _isDataStackVisible, value);
        }

        #endregion

        #region Constructors

        public LoginPageViewModel()
        {
            _logInUserModel = new LogInUserModel
            {
                Username = "rest",
                Password = "rest"
            };
        }

        #endregion

        #region Commands

        public IAsyncValueCommand LoginCommand => new AsyncValueCommand(Login);

        public ICommand SignUpCommand => new Command(() => Device.BeginInvokeOnMainThread(() => Navigation.PushAsync(new SignAsRestaurantPage()).SafeFireAndForget(ex => Helpers.LogError(ex))));

        public ICommand ForgotPasswordCommand { get; set; }

        #endregion

        #region Methods

        private async ValueTask Login()
        {
            IsDataStackVisible = false;
            var cToken = EatUppApp.CToken;

            try
            {
                var userSerialized = JsonConvert.SerializeObject(_logInUserModel);
                var content = new StringContent(userSerialized, Encoding.UTF8, "application/json");
                var responseMessage = await HttpClient.PostAsync($"{_url}{_endpoint}", content, cToken.Token);
                cToken.Dispose();

                if (!responseMessage.IsSuccessStatusCode)
                    throw new Exception(responseMessage.ReasonPhrase);

                var jsonResponse = await responseMessage.Content?.ReadAsStringAsync();
                var response = JsonConvert.DeserializeObject<SignInResponse>(jsonResponse);
                if (response.Error)
                    throw new Exception(response.ErrorResponse.ErrorDescription);

                EatUppApp.Properties[PropertyKeys.RestaurantId.ToString()] = response.Id;
                EatUppApp.Properties[PropertyKeys.Token.ToString()] = response.Token;
                EatUppApp.SavePropertiesAsync().SafeFireAndForget(ex => Helpers.LogError(ex));

                SignalR.ConnectSignalR().SafeFireAndForget(ex => Helpers.LogError(ex));

                EatUppApp.MainPage = new REatUppShell();
            }
            catch (OperationCanceledException ex)
            {
                cToken.Dispose();
                IsDataStackVisible = true;
                Helpers.LogError(ex);
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Operation is canceled", "Try again", "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
            }
            catch (Exception ex)
            {
                cToken.Dispose();
                IsDataStackVisible = true;
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert(ex.Message, "Try again", "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
        }

        #endregion
    }
}