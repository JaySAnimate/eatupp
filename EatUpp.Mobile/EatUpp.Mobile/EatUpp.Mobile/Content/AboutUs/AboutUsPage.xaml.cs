﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutUsPage : ContentPage
    {
        public AboutUsPage()
        {
            InitializeComponent();
        }
    }
}