﻿using System;
using Newtonsoft.Json;
using EatUpp.Mobile.Misc;
using System.Threading.Tasks;
using AsyncAwaitBestPractices;
using EatUpp.Mobile.Models.Data;
using AsyncAwaitBestPractices.MVVM;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using EatUpp.Mobile.Common.Enums;
using System.Net.Http.Headers;
using System.Windows.Input;

namespace EatUpp.Mobile.Content
{
    public class RestaurantsPageViewModel : BaseViewModel
    {
        #region Private Fields

        private readonly string _endpoint = "/api/Customer/GetRestaurants";

        #endregion

        #region Properties

        private ObservableCollection<Restaurant> _restaurants;
        public ObservableCollection<Restaurant> Restaurants
        {
            get => _restaurants;
            set => SetValue(ref _restaurants, value);
        }

        private Restaurant _selectedRestaurant;
        public Restaurant SelectedRestaurant
        {
            get => _selectedRestaurant;
            set => SetValue(ref _selectedRestaurant, value);
        }

        private bool _isLoading;
        public bool IsLoading 
        { 
            get => _isLoading; 
            set => SetValue(ref _isLoading, value); 
        }

        #endregion

        public RestaurantsPageViewModel()
        {
            GetRestaurants().SafeFireAndForget(ex => Helpers.LogError(ex));
        }

        #region Commands

        public ICommand OpenMenuCommand => new Command<int>(id => Device.BeginInvokeOnMainThread(() => Navigation.PushAsync(new MenuPage(id)).SafeFireAndForget(ex => Helpers.LogError(ex))));

        #endregion

        #region Methods

        private async Task GetRestaurants()
        {
            try
            {
                var cToken = EatUppApp.CToken;
                var responseMessage = await HttpClient.GetAsync($"{_url}{_endpoint}", cToken.Token);
                cToken.Dispose();

                if (!responseMessage.IsSuccessStatusCode)
                    throw new Exception(responseMessage.ReasonPhrase);

                var jsonResponse = await responseMessage.Content.ReadAsStringAsync();
                Restaurants = JsonConvert.DeserializeObject<ObservableCollection<Restaurant>>(jsonResponse);
            }
            catch (Exception ex)
            {
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Crash", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
        }

        #endregion
    }
}