﻿using AsyncAwaitBestPractices;
using EatUpp.Mobile.Common.Enums;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RestaurantsPage : ContentPage
    {
        public RestaurantsPage()
        {
            InitializeComponent();
            BindingContext = new RestaurantsPageViewModel();
        }
    }
}