﻿using System;
using System.Text;
using Xamarin.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using EatUpp.Mobile.Misc;
using EatUpp.Mobile.Models;
using System.Threading.Tasks;
using AsyncAwaitBestPractices.MVVM;
using EatUpp.Mobile.Models.Response;
using AsyncAwaitBestPractices;
using System.Windows.Input;

namespace EatUpp.Mobile.Content
{
    public class SignAsRestaurantViewModel : BaseViewModel
    {
        #region Private Fields

        private const string _endpoint = "/api/User/SignUpRestaurant";

        #endregion

        #region Properties

        private SignAsRestaurantModel _signAsRestaurantModel;
        public SignAsRestaurantModel SignAsRestaurantModel
        {
            get => _signAsRestaurantModel;
            set => SetValue(ref _signAsRestaurantModel, value);
        }

        private bool _isDataStackVisible = true;
        public bool IsDataStackVisible
        {
            get => _isDataStackVisible;
            set => SetValue(ref _isDataStackVisible, value);
        }

        #endregion

        public SignAsRestaurantViewModel()
        {
            _signAsRestaurantModel = new SignAsRestaurantModel();
        }

        #region Commands

        public IAsyncCommand SignUpCommand => new AsyncCommand(SignUp);

        public ICommand LogInCommand => new Command(() => Device.BeginInvokeOnMainThread(() => Navigation.PopAsync().SafeFireAndForget(ex => Helpers.LogError(ex))));

        #endregion

        #region Methods

        private async Task SignUp()
        {
            IsDataStackVisible = false;
            var cToken = EatUppApp.CToken;
            try
            {
                var signUpModelSerialized = JsonConvert.SerializeObject(SignAsRestaurantModel);
                var content = new StringContent(signUpModelSerialized, Encoding.UTF8, "application/json");
                var responseMessage = await HttpClient.PostAsync($"{_url}{_endpoint}", content, cToken.Token);
                cToken.Dispose();

                if (!responseMessage.IsSuccessStatusCode)
                    throw new Exception(responseMessage.ReasonPhrase);

                var jsonResponse = await responseMessage.Content?.ReadAsStringAsync();
                var response = JsonConvert.DeserializeObject<SignUpResponse>(jsonResponse);
                if (response.Error)
                    throw new Exception(response.ErrorResponse.ErrorDescription);

                Device.BeginInvokeOnMainThread(() => Navigation.PopAsync().SafeFireAndForget(ex => Helpers.LogError(ex)));
            }
            catch (Exception ex)
            {
                cToken.Dispose();
                IsDataStackVisible = true;
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    EatUppApp.MainPage.DisplayAlert("Crash", ex.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
        }

        #endregion

    }
}
