﻿using Xamarin.Forms;
using System.Net.Http;
using Xamarin.Essentials;
using EatUpp.Mobile.Models;
using System.Net.Http.Headers;
using Xamarin.Forms.Internals;
using EatUpp.Mobile.Common.Enums;
using System.Runtime.Serialization;

namespace EatUpp.Mobile.Content
{
    [Preserve(AllMembers = true)]
    [DataContract]
    public partial class BaseViewModel : BaseModel
    {
        #region Private Fields

        private static HttpClient _httpClient;

        public const string _url = "https://eatupp-api-vc3.conveyor.cloud";

        #endregion        

        public BaseViewModel() => Connectivity.ConnectivityChanged += (sender, args) => NotifyPropertyChanged(nameof(NetworkAccessLost));

        #region Properties

        public static INavigation Navigation => EatUppApp.MainPage.Navigation;

        public static App EatUppApp => Application.Current as App;

        public static HttpClientHandler HttpHandler => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true };

        public bool NetworkAccessLost => Connectivity.NetworkAccess != NetworkAccess.Internet;

        public static HttpClient HttpClient
        {
            get
            {
                if (_httpClient == null)
                    _httpClient = new HttpClient(HttpHandler);

                if (EatUppApp.Properties.ContainsKey(PropertyKeys.Token.ToString()))
                    _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", EatUppApp.Properties[PropertyKeys.Token.ToString()].ToString());

                return _httpClient;
            }
        }

        #endregion
    }
}