﻿using AsyncAwaitBestPractices;
using EatUpp.Mobile.Misc;
using EatUpp.Mobile.Models.Data;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using MenuItem = EatUpp.Mobile.Models.Data.MenuItem;

namespace EatUpp.Mobile.Converters
{
    public class FilterMenuByCategoryConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (values.Length != 2 || !(values[0] is ObservableCollection<MenuItem> menu) || !(values[1] is FoodCategoryData category))
                    return null;

                return menu.Where(item => item.CategoryId == category.Id);
            }
            catch
            {
                Task.Run(() => Helpers.LogError(new Exception($"Invalid Values Passed To {GetType().Name}: {JsonConvert.SerializeObject(values)}"))).SafeFireAndForget(ex => Helpers.LogError(ex));
                return null;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}