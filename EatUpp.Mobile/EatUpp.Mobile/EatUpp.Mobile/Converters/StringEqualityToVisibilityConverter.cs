﻿using AsyncAwaitBestPractices;
using EatUpp.Mobile.Misc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EatUpp.Mobile.Converters
{
    public class StringEqualityToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                bool areEqual = false;

                if (values.Length != 2)
                    return areEqual;

                areEqual = values[0].ToString() == values[1].ToString();

                if (parameter is string invert && invert == "!")
                    areEqual = !areEqual;

                return areEqual;
            }
            catch
            {
                Task.Run(() => Helpers.LogError(new Exception($"Invalid Values Passed To {GetType().Name}: {JsonConvert.SerializeObject(values)}"))).SafeFireAndForget(ex => Helpers.LogError(ex));
                return false;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
