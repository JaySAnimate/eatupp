﻿using System;
using Xamarin.Forms;
using System.Globalization;

namespace EatUpp.Mobile.Converters
{
    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            parameter is string invert && invert == "!" ? value is null : !(value is null);

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException("NullToVisibilityConverter: ConvertBack");
    }
}
