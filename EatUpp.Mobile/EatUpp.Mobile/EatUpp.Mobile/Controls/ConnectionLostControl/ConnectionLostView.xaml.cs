﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConnectionLostView : ContentView
    {
        public ConnectionLostView()
        {
            InitializeComponent();
        }
    }
}