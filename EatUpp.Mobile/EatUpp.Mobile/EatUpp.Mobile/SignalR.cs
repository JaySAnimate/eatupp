﻿using System;
using Xamarin.Forms;
using System.Threading;
using System.Diagnostics;
using EatUpp.Mobile.Misc;
using Xamarin.Essentials;
using System.Threading.Tasks;
using EatUpp.Mobile.Models.Data;
using EatUpp.Mobile.Common.Enums;
using Microsoft.AspNetCore.SignalR.Client;
using AsyncAwaitBestPractices;
using EatUpp.Mobile.Content;

namespace EatUpp.Mobile
{
    public static class SignalR
    {
        #region Private Fields

        private const string _endpoint = "/EatHub?JWTToken=";

        #endregion

        #region Events

        public static event Action<int> OnCustomerPoked;
        public static event Action<int> OnNewRestaurantOnline;
        public static event Action<OrderInfo> OnOrderReceived;

        #endregion

        #region Properties

        public static HubConnection HubConnection { get; set; }

        public static App EatUppApp => (Application.Current as App);

        #endregion

        public static async Task<HubConnectionState> ConnectSignalR()
        {
            var cToken = EatUppApp.CToken;
            try
            {
                HubConnection = new HubConnectionBuilder()
                    .WithUrl($"{BaseViewModel._url}{_endpoint}{EatUppApp.Properties[PropertyKeys.Token.ToString()]}", options => options.HttpMessageHandlerFactory += unsafeHandler => BaseViewModel.HttpHandler)
                    .WithAutomaticReconnect()
                    .Build();

                HubConnection.KeepAliveInterval = TimeSpan.FromMinutes(30);

                RegisterFunctions();

                await HubConnection.StartAsync(cToken.Token);
                cToken.Dispose();
            }
            catch
            {
                cToken.Dispose();
                throw;
            }

            return HubConnection.State;
        }

        public static void RegisterFunctions()
        {
            HubConnection.Closed += OnConnectionClosed;
            HubConnection.Reconnected += OnReconnected;
            HubConnection.Reconnecting += OnReconnecting;

            HubConnection.On<string>(SROn.Connected.ToString(), Connected);
            HubConnection.On<int>(SROn.CustomerPoked.ToString(), CustomerPoked);
            HubConnection.On<string>(SROn.Disconnected.ToString(), Disconnected);
            HubConnection.On<int>(SROn.RejectionReceived.ToString(), RejectBooking);
            HubConnection.On<int>(SROn.ApprovementReceived.ToString(), ApproveBooking);
            HubConnection.On<BookingData>(SROn.BookReceived.ToString(), BookingReceived);
            HubConnection.On<OrderInfo>(SROn.FoodOrderReceived.ToString(), ReceiveFoodOrder);
            HubConnection.On<MessageModel>(SROn.MessageReceived.ToString(), MessageReceived);
            HubConnection.On<int>(SROn.NotifyRestaurantOnlineStatus.ToString(), NotifyRestaurantOnlineStatus);
        }

        #region Connection Events

        private static Task OnReconnecting(Exception ex) => Task.Run(() => Helpers.LogError(ex));

        private static Task OnReconnected(string connectionId) => Task.Run(() => Debug.WriteLine(connectionId));

        private static Task OnConnectionClosed(Exception ex) => Task.Run(() => Helpers.LogError(ex));

        #endregion

        #region Common On

        public static void Connected(string userSRID) => MainThread.BeginInvokeOnMainThread(() => (Application.Current as App).MainPage.DisplayAlert("Connected", userSRID, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));

        public static void Disconnected(string connectionId) => Debug.WriteLine(connectionId);

        public static void MessageReceived(MessageModel messageModel) => MainThread.BeginInvokeOnMainThread(() => (Application.Current as App).MainPage.DisplayAlert("Message", messageModel.Message, "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));

        #endregion

        #region Restaurant On

        public static void CustomerPoked(int tableId) => OnCustomerPoked?.Invoke(tableId);

        public static void NotifyRestaurantOnlineStatus(int restaurantId) => OnNewRestaurantOnline?.Invoke(restaurantId);

        public static void ReceiveFoodOrder(OrderInfo orderInfo) => OnOrderReceived?.Invoke(orderInfo);

        public static void BookingReceived(BookingData obj) => throw new NotImplementedException();

        public static void ApproveBooking(int bookingId) => throw new NotImplementedException();

        public static void RejectBooking(int bookingId) => throw new NotImplementedException();

        #endregion
    }
}