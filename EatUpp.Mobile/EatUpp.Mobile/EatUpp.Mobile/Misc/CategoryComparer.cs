﻿using EatUpp.Mobile.Models.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Mobile.Misc
{
    public class CategoryComparer : IEqualityComparer<FoodCategoryData>
    {
        public bool Equals(FoodCategoryData x, FoodCategoryData y) => x.Category.Equals(y.Category);

        public int GetHashCode(FoodCategoryData category) => category.Category.GetHashCode();
    }
}