﻿using AsyncAwaitBestPractices;
using EatUpp.Mobile.Models.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace EatUpp.Mobile.Misc
{
    public static class Helpers
    {
        public static void LogError(object error, bool writeLog = true)
        {
            if (error is Exception ex)
            {
                string exMessage = $"Message: {ex.Message}\n" +
                                   $"IneerExType: {ex.InnerException?.GetType().Name}\n" +
                                   $"InnerMessage: {ex.InnerException?.Message}\n";

                Debug.WriteLine(exMessage);
            }

            if (writeLog)
                Task.Run(() =>
                {
                    // TODO: Request backend to write log on server
                    Console.WriteLine($"{DateTime.Now}: {JsonConvert.SerializeObject(error)}");
                }).SafeFireAndForget(ex => LogError(ex, false));
        }
    }
}
