﻿using Xamarin.Forms.Xaml;

namespace EatUpp.Mobile.MiscXAML
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Converters
    {
        public Converters()
        {
            InitializeComponent();
        }
    }
}