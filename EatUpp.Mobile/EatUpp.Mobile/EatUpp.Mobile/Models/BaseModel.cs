﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace EatUpp.Mobile.Models
{
    public class BaseModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName) =>
          PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        protected void SetValue<T>(ref T field, T value, [CallerMemberName]string propertyName = null)
        {
            if (!(field is null) && field.Equals(value))
                return;

            field = value;
            NotifyPropertyChanged(propertyName);
        }
    }
}