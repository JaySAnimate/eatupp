﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace EatUpp.Mobile.Models.Data
{
    public class OrderInfo
    {
        public int TableId { get; set; }

        public DateTime OrderTime { get; set; }

        public string Order { get; set; }
    }
}
