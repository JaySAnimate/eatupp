﻿namespace EatUpp.Mobile.Models.Data
{
    class PostCard
    {
        public string RestaurantId { get; set; }

        public int TableId { get; set; }
    }
}