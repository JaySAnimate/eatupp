﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace EatUpp.Mobile.Models.Data
{
    public static class CollectedOrder
    {
        private static ObservableCollection<MenuItem> _collectedOrder;

        public static int OrderItemsQuantity
        {
            get
            {
                if (_collectedOrder is null)
                    return 0;

                var quantity = 0;

                foreach (var item in _collectedOrder)
                    quantity += item.Count;

                return quantity;
            }
        }

        public static ref ObservableCollection<MenuItem> GetCollectedOrder()
        {
            if (_collectedOrder is null)
                _collectedOrder = new ObservableCollection<MenuItem>();

            return ref _collectedOrder;
        }
    }
}