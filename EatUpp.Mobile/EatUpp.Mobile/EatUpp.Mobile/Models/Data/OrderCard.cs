﻿using System;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace EatUpp.Mobile.Models.Data
{
    public class OrderCard
    {
        public OrderCard() { }
         
        public OrderCard(OrderInfo orderInfo)
        {
            TableId = orderInfo.TableId;
            OrderTime = orderInfo.OrderTime;
            Order = JsonConvert.DeserializeObject<ObservableCollection<MenuItem>>(orderInfo.Order);
        }

        public int TableId { get; set; }

        public DateTime OrderTime { get; set; }

        public ObservableCollection<MenuItem> Order { get; set; }
    }
}
