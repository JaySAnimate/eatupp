﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Mobile.Models.Data
{
    public class MessageModel
    {
        public string FromName { get; set; }

        public string Message { get; set; }

        public override string ToString() => $"From: {FromName}\nMessage: {Message}\n";
    }
}
