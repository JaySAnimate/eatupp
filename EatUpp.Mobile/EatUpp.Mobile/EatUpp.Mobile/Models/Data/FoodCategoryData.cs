﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatUpp.Mobile.Models.Data
{
    public class FoodCategoryData
    {
        public int Id { get; set; }
        public string Category { get; set; }

        public override string ToString() => Category;
    }
}
