﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace EatUpp.Mobile.Models.Data
{
    public class MenuItem:BaseModel
    {
        public int Id { get; set; }
        public int RestaurantId { get; set; }

        [JsonProperty("FoodCategoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("FoodName")]
        public string Name { get; set; }

        [JsonProperty("FoodPrice")]
        public decimal? Price { get; set; }

        [JsonProperty("FoodDescription")]
        public string Description { get; set; }

        public byte[] FoodImage { get; set; }

        private int _count;
        [DataMember]
        public int Count
        {
            get => _count;
            set => SetValue(ref _count, value);
        }

        public FoodCategoryData FoodCategory { get; set; }
    }
}
