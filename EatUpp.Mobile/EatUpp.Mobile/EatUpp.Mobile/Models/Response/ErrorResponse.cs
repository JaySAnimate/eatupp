﻿using EatUpp.Mobile.Common.Enums;

namespace EatUpp.Mobile.Models.Response
{
    public struct ErrorResponse
    {
        public ErrorType ErrorType { get; set; }

        public string ErrorDescription { get; set; }
    }
}