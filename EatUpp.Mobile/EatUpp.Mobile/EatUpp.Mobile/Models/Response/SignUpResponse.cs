﻿using EatUpp.Mobile.Models.Response;

namespace EatUpp.Mobile.Models.Response
{
    public struct SignUpResponse
    {
        public string Email { get; set; }

        public int RoleId { get; set; }

        public string Token { get; set; }

        public bool Error { get; set; }

        public ErrorResponse ErrorResponse { get; set; }
    }
}