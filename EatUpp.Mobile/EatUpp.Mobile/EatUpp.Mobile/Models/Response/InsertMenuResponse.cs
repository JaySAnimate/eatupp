﻿using System.Collections.Generic;

namespace EatUpp.Mobile.Models.Response
{
    public class InsertionResponse
    {
        public int Status { get; set; }

        public bool Error { get; set; }

        public IEnumerable<object> Absents { get; set; }

        public ErrorResponse ErrorResponse { get; set; }
    }
}
