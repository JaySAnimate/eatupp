﻿namespace EatUpp.Mobile.Models.Response
{
    public struct SignInResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Role { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }

        public bool Error { get; set; }

        public ErrorResponse ErrorResponse { get; set; }
    }
}