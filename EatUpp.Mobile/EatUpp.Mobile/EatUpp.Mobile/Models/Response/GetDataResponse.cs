﻿namespace EatUpp.Mobile.Models.Response
{
    public class GetDataResponse<T>
    {
        public bool Error { get; set; }

        public ErrorResponse ErrorResponse { get; set; }

        public T Data { get; set; }
    }
}
