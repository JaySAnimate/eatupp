﻿using System.Runtime.Serialization;

namespace EatUpp.Mobile.Models.Data
{
    public class SignUpCustomerModel : BaseModel
    {
        private string _firstname;
        [DataMember]
        public string Firstname
        {
            get => _firstname;
            set => SetValue(ref _firstname, value);
        }

        private string _lastname;
        [DataMember]
        public string Lastname
        {
            get => _lastname;
            set => SetValue(ref _lastname, value);
        }

        private string _username;
        [DataMember]
        public string Username
        {
            get => _username;
            set => SetValue(ref _username, value);
        }

        private string _password;
        [DataMember]
        public string Password
        {
            get => _password;
            set => SetValue(ref _password, value);
        }

        private string _confirmPassword;
        [DataMember]
        public string ConfirmPassword
        {
            get => _confirmPassword;
            set => SetValue(ref _confirmPassword, value);
        }

        private string _phone;
        [DataMember]
        public string Phone
        {
            get => _phone;
            set => SetValue(ref _phone, value);
        }

        private string _email;
        [DataMember]
        public string Email
        {
            get => _email;
            set => SetValue(ref _email, value);
        }
    }
}