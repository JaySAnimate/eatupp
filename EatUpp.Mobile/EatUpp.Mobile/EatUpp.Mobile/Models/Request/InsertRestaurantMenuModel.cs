﻿using EatUpp.Mobile.Common.Enums;
using System.Runtime.Serialization;
using Xamarin.Forms;

namespace EatUpp.Mobile.Models.Request
{
    public class InsertRestaurantMenuModel : BaseModel
    {
        [DataMember]
        public int RestaurantId { get => (int)(Application.Current as App).Properties[PropertyKeys.RestaurantId.ToString()]; }

        public int _foodCategoryId;
        [DataMember]
        public int FoodCategoryId
        {
            get => _foodCategoryId;
            set => SetValue(ref _foodCategoryId, value);
        }

        public string _foodName;
        [DataMember]
        public string FoodName
        {
            get => _foodName;
            set => SetValue(ref _foodName, value);
        }

        public decimal? _foodPrice;
        [DataMember]
        public decimal? FoodPrice
        {
            get => _foodPrice;
            set => SetValue(ref _foodPrice, value);
        }

        public string _foodDescription;
        [DataMember]
        public string FoodDescription
        {
            get => _foodDescription;
            set => SetValue(ref _foodDescription, value);
        }

        public byte[] _foodImage;
        [DataMember]
        public byte[] FoodImage
        {
            get => _foodImage;
            set => SetValue(ref _foodImage, value);
        }
    }
}
