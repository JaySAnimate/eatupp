﻿using System;

namespace EatUpp.Mobile.Models.Request
{
    public class GetOrderRequest
    {
        public int RestaurantId { get; set; }

        public DateTime? From { get; set; }
    }
}
