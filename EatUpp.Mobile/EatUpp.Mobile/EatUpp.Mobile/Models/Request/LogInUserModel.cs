﻿using System.Runtime.Serialization;

namespace EatUpp.Mobile.Models.Request
{
    public class LogInUserModel : BaseModel
    {
        public string _username;
        [DataMember]
        public string Username
        {
            get => _username;
            set { SetValue(ref _username, value); }
        }

        public string _password;
        [DataMember]
        public string Password
        {
            get => _password;
            set => SetValue(ref _password, value);
        }
    }
}