﻿namespace EatUpp.Mobile.Models
{
    public class SignAsRestaurantModel : BaseModel
    {
        private string _login;
        public string Login
        {
            get => _login;
            set => SetValue(ref _login, value);
        }

        private string _password;
        public string Password
        {
            get => _password;
            set => SetValue(ref _password, value);
        }

        private string _confirmPassword;
        public string ConfirmPassword
        {
            get => _confirmPassword;
            set => SetValue(ref _confirmPassword, value);
        }

        private string _restaurantname;
        public string RestaurantName
        {
            get => _restaurantname;
            set => SetValue(ref _restaurantname, value);
        }

        private string _restaurantDescription;//n
        public string RestaurantDescription
        {
            get => _restaurantDescription;
            set => SetValue(ref _restaurantDescription, value);
        }
        private string _phone;
        public string Phone
        {
            get => _phone;
            set => SetValue(ref _phone, value);
        }

        private string _address;
        public string Address
        {
            get => _address;
            set => SetValue(ref _address, value);
        }

        private bool _rVIPZone;//n
        public bool RVIPZone
        {
            get => _rVIPZone;
            set => SetValue(ref _rVIPZone, value);
        }

        private bool _rNonSmokingZone;//n
        public bool RNonSmokingZone
        {
            get => _rNonSmokingZone;
            set => SetValue(ref _rNonSmokingZone, value);
        }

        private string _email;
        public string Email
        {
            get => _email;
            set => SetValue(ref _email, value);
        }

    }
}
