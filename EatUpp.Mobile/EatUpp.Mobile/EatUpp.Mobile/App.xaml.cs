﻿using AsyncAwaitBestPractices;
using EatUpp.Mobile.Common.Enums;
using EatUpp.Mobile.Content;
using EatUpp.Mobile.Misc;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace EatUpp.Mobile
{
    public partial class App : Application
    {
        public CancellationTokenSource CToken
        {
            get
            {

                var _cToken = new CancellationTokenSource((int)TimeSpan.FromSeconds(30).TotalMilliseconds);
                _cToken.Token.ThrowIfCancellationRequested();
                _cToken.Token.Register(() =>
                {
                    try
                    {
                        Device.BeginInvokeOnMainThread(() =>
                            MainPage.DisplayAlert("Connection timed out", "Slow Connection...\nRequest Takes too long(>30sec).\nTry Again", "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
                    }
                    catch (Exception ex)
                    {
                        Helpers.LogError(ex);
#if DEBUG
                        Device.BeginInvokeOnMainThread(() =>
                            MainPage.DisplayAlert(ex.Message, "Try again", "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
                    }
                }, false);

                return _cToken;
            }
        }

        public App()
        {
            InitializeComponent();

            try
            {
                if (!Properties.ContainsKey(PropertyKeys.Token.ToString()))
                {
                    MainPage = new NavigationPage(new FastSign());
                    return;
                }

                var token = ReadToken(Properties[PropertyKeys.Token.ToString()].ToString());
                if (token.ValidTo < DateTime.Now.AddMinutes(5.0))
                {
                    MainPage = new NavigationPage(new FastSign());
                    return;
                }

                SignalR.ConnectSignalR().SafeFireAndForget(ex => Helpers.LogError(ex));

                var role = token.Claims.Where(claim => claim.Type == "role").FirstOrDefault().Value;
                if (role == Role.Customer.ToString())
                    MainPage = new CEatUppShell();
                else
                    MainPage = new REatUppShell();
            }
            catch (OperationCanceledException ex)
            {
                MainPage = new NavigationPage(new FastSign());
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    MainPage.DisplayAlert(ex.Message, "Try again", "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
            catch (Exception ex)
            {
                MainPage = new NavigationPage(new FastSign());
                Helpers.LogError(ex);
#if DEBUG
                Device.BeginInvokeOnMainThread(() =>
                    MainPage.DisplayAlert(ex.Message, "Try again", "Ok").SafeFireAndForget(ex => Helpers.LogError(ex)));
#endif
            }
        }

        public static JwtSecurityToken ReadToken(string encodedToken)
        {
            try
            {
                return string.IsNullOrEmpty(encodedToken) ? throw new Exception("Token Cannot be empty") : new JwtSecurityTokenHandler().ReadToken(encodedToken) as JwtSecurityToken;
            }
            catch { throw; }
        }
    }
}