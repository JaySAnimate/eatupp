﻿using Android.OS;
using Android.App;
using Android.Content.PM;
using EatUpp.Mobile.Droid;

namespace Awesomekit.Droid
{
    [Activity(Label = "EatUpp", Icon = "@mipmap/EatUpp_Logo", Theme = "@style/SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            StartActivity(typeof(MainActivity));
            Finish();
        }
    }
}