﻿using Android.OS;
using Android.App;
using Xamarin.Forms;
using Android.Runtime;
using Android.Content.PM;
using ZXing.Net.Mobile.Android;
using Xamarin.Forms.Platform.Android;

namespace EatUpp.Mobile.Droid
{
    [Activity(Label = "EatUpp", Icon = "@mipmap/EatUpp_Logo", Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            ZXing.Net.Mobile.Forms.Android.Platform.Init();
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            Forms.SetFlags("Expander_Experimental");
            Forms.Init(this, savedInstanceState);

            LoadApplication(new App());
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}