﻿using AutoMapper;
using EatUpp.DTO;
using EatUpp.DTO.Models;
using EatUpp.DAL.Entities;
using EatUpp.DTO.Models.Data;
using EatUpp.DTO.Models.Request;

namespace EatUpp.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Model -> Entity <- Model

            CreateMap<RestaurantModel, Restaurant>().ReverseMap();

            CreateMap<CustomerModel, Customer>().ReverseMap();

            CreateMap<UserModel, User>().ReverseMap();

            CreateMap<SignalRIdentifierModel, SignalRIdentifier>().ReverseMap();

            CreateMap<BookRequest, Book>().ReverseMap();

            #endregion

            #region Request -> Entity

            CreateMap<RestaurantRequest, Restaurant>()
                .ForPath(entity => entity.User.Login, dest => dest.MapFrom(model => model.Login))
                .ForPath(entity => entity.User.Password, dest => dest.MapFrom(model => model.Password));

            CreateMap<CustomerRequest, Customer>()
                .ForPath(entity => entity.User.Login, dest => dest.MapFrom(model => model.Login))
                .ForPath(entity => entity.User.Password, dest => dest.MapFrom(model => model.Password));

            CreateMap<BookRequest, Book>().ReverseMap();

            CreateMap<MenuItemRequest, MenuItem>().ReverseMap();

            #endregion

            #region Data -> Entity

            CreateMap<BookingData, Book>()
                .ForMember(entity => entity.Customer, data => data.Ignore())
                .ForMember(entity => entity.Restaurant, data => data.Ignore());

            CreateMap<MenuItemData, MenuItem>()
                .ForMember(entity => entity.Restaurant, data => data.Ignore())
                .ForMember(entity => entity.FoodCategory, data => data.Ignore());

            CreateMap<FoodOrderData, FoodOrder>()
                .ForMember(entity => entity.OrderByMenu, dest => dest.MapFrom(data => data.Order))
                .ForMember(entity => entity.Restaurant, data => data.Ignore());

            #endregion

            #region Entity -> Data <- Entity

            CreateMap<Restaurant, RestaurantData>().ReverseMap();

            CreateMap<Customer, CustomerData>().ReverseMap();

            CreateMap<FoodCategory, FoodCategoryData>().ReverseMap();

            #endregion

            #region Entity -> Data 

            CreateMap<MenuItem, MenuItemData>();

            CreateMap<Book, BookingData>();

            CreateMap<FoodOrder, FoodOrderData>()
                .ForMember(data => data.Order, dest => dest.MapFrom(entity => entity.OrderByMenu));

            #endregion

            #region Model -> Data <- Model

            CreateMap<RestaurantModel, RestaurantData>().ReverseMap();

            CreateMap<CustomerModel, CustomerData>().ReverseMap();

            #endregion
        }
    }
}