﻿using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace EatUpp.API.Extensions
{
    public class EatUppJwtSecurityTokenHandler : JwtSecurityTokenHandler
    {
        public override ClaimsPrincipal ValidateToken(string token, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            validatedToken = new JwtSecurityToken(token);
            var identity = CreateClaimsIdentity((JwtSecurityToken)validatedToken, validatedToken.Issuer, validationParameters);

            return new ClaimsPrincipal(identity);
        }
    }
}