﻿using EatUpp.BLL;
using EatUpp.DAL.UnitOfEatUpp;
using EatUpp.DAL.EntityRepositories;
using EatUpp.DAL.EntityRepositoryInterfaces;
using Microsoft.Extensions.DependencyInjection;

namespace EatUpp.API.Extensions
{
    public static class DependencyExtention
    {
        public static void AddEatUppDependancy(this IServiceCollection services)
        {
            services.AddScoped(typeof(ISignalRIdentifierRepository), typeof(SignalRIdentifierRepository));
            services.AddScoped(typeof(IRestaurantRepository), typeof(RestaurantRepository));
            services.AddScoped(typeof(IMenuItemsRepository), typeof(MenuItemsRepository));
            services.AddScoped(typeof(IFoodOrderRepository), typeof(FoodOrderRepository));
            services.AddScoped(typeof(ICustomerRepository), typeof(CustomerRepository));
            services.AddScoped(typeof(IUserRoleRepository), typeof(UserRoleRepository));
            services.AddScoped(typeof(IEventRepository), typeof(EventRepository));
            services.AddScoped(typeof(IOrderRepository), typeof(OrderRepository));
            services.AddScoped(typeof(IEmailRepository), typeof(EmailRepository));
            services.AddScoped(typeof(IRoleRepository), typeof(RoleRepository));
            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));

            services.AddScoped(typeof(IUnitOfEatUpp), typeof(UnitOfEatUpp));
            services.AddScoped(typeof(EatUppManager), typeof(EatUppManager));
        }
    }
}