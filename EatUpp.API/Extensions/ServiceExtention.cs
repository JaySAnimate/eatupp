﻿using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace EatUpp.API.Extensions
{
    public static class ServiceExtention
    {
        public static void AddJWT(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(config =>
            {
                config.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                config.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(config =>
            {
                config.SecurityTokenValidators.Add(new EatUppJwtSecurityTokenHandler());
                config.Events = new JwtBearerEvents { OnMessageReceived = HandleToken };
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;
                config.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration.GetValue<string>("JWTKey")))
                };
            });
        }

        private static Task HandleToken(MessageReceivedContext context)
        {
            var accessToken = context.Request.Query["JWTToken"];
            if (!string.IsNullOrEmpty(accessToken))
                context.Token = accessToken;

            return Task.CompletedTask;
        }
    }
}