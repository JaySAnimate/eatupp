﻿using System;
using EatUpp.BLL;
using System.Linq;
using EatUpp.DTO.Models;
using System.Threading.Tasks;
using EatUpp.DTO.Models.Data;
using System.Security.Claims;
using EatUpp.Common.Constants;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;

namespace EatUpp.API.SignalRHubs
{
    [Authorize]
    public class EatHub : Hub
    {
        private readonly EatUppManager _manager;
        public EatHub(EatUppManager manager) => _manager = manager;

        public override async Task OnConnectedAsync()
        {
            var role = Context.User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Role).Value;

            await Groups.AddToGroupAsync(Context.ConnectionId, role);

            if (role == Role.Restaurant)
                await Clients.Group(Role.Customer).SendAsync(RemoteFunctions.NotifyRestaurantOnlineStatus, Context.UserIdentifier);

            await Clients.User(Context.UserIdentifier).SendAsync("Connected", Context.ConnectionId);
            Console.WriteLine("Connected: {0}", Context.UserIdentifier);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, Context.User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Role).Value);
            await Clients.User(Context.UserIdentifier).SendAsync(RemoteFunctions.Disconnected, Context.ConnectionId);
            Console.WriteLine("Disconnected: {0}", Context.UserIdentifier);
        }

        public async Task SendMessage(string to, MessageModel message) => await Clients.User(to).SendAsync(RemoteFunctions.MessageReceived, message);

        [Authorize(Roles = Role.Customer)]
        public async Task PlaceBook(string to, BookingData order) => await Clients.User(to).SendAsync(RemoteFunctions.BookReceived, order);

        [Authorize(Roles = Role.Customer)]
        public async Task PokeRestaurant(PostCard postCard) =>
            await Clients.User(_manager.GetRestaurantSRID(int.Parse(postCard.RestaurantId))).SendAsync(RemoteFunctions.CustomerPoked, postCard.TableId);

        [Authorize(Roles = Role.Customer)]
        public async Task<bool> OrderFood(FoodOrderData orderData)
        {
            var saved = _manager.SaveFoodOrder(orderData);
            if (saved == 0)
                return false;

            var order = new OrderInfoForRestaurant
            {
                Order = orderData.Order,
                OrderTime = orderData.OrderTime,
                TableId = orderData.TableId
            };

            await Clients.User(_manager.GetRestaurantSRID(orderData.RestaurantId)).SendAsync(RemoteFunctions.FoodOrderReceived, order);
            return true;
        }

        [Authorize(Roles = Role.Restaurant)]
        public async Task ApproveBook(string to, int orderId) => await Clients.User(to).SendAsync(RemoteFunctions.ApprovementReceived, orderId);

        [Authorize(Roles = Role.Restaurant)]
        public async Task RejectBook(string to, int orderId) => await Clients.User(to).SendAsync(RemoteFunctions.RejectionReceived, orderId);
    }
}