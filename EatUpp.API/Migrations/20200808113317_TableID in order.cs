﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EatUpp.API.Migrations
{
    public partial class TableIDinorder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TableId",
                table: "FoodOrders",
                type: "INT",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TableId",
                table: "FoodOrders");
        }
    }
}
