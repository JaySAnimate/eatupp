﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EatUpp.API.Migrations
{
    public partial class RemoveSignalRIdentifierIdfromRestaurant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Restaurants_SignalRIdentifiers_SignalRIdentifierId",
                table: "Restaurants");

            migrationBuilder.DropIndex(
                name: "IX_Restaurants_SignalRIdentifierId",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "SignalRIdentifierId",
                table: "Restaurants");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SignalRIdentifierId",
                table: "Restaurants",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Restaurants_SignalRIdentifierId",
                table: "Restaurants",
                column: "SignalRIdentifierId");

            migrationBuilder.AddForeignKey(
                name: "FK_Restaurants_SignalRIdentifiers_SignalRIdentifierId",
                table: "Restaurants",
                column: "SignalRIdentifierId",
                principalTable: "SignalRIdentifiers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
