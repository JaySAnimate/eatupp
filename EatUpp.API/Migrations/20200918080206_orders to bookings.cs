﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EatUpp.API.Migrations
{
    public partial class orderstobookings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RestaurantId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    ChairCount = table.Column<int>(type: "INT", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    CheckDate = table.Column<DateTime>(type: "DATETIME", nullable: true),
                    CancelDate = table.Column<DateTime>(type: "DATETIME", nullable: true),
                    VIPZone = table.Column<bool>(type: "BIT", nullable: false),
                    NonSmokingZone = table.Column<bool>(type: "BIT", nullable: false),
                    Comments = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Approved = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bookings_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Bookings_Restaurants_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_CustomerId",
                table: "Bookings",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_RestaurantId",
                table: "Bookings",
                column: "RestaurantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Approved = table.Column<int>(type: "INT", nullable: false),
                    CancelDate = table.Column<DateTime>(type: "DATETIME", nullable: true),
                    ChairCount = table.Column<int>(type: "INT", nullable: false),
                    CheckDate = table.Column<DateTime>(type: "DATETIME", nullable: true),
                    Comments = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    NonSmokingZone = table.Column<bool>(type: "BIT", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    RestaurantId = table.Column<int>(type: "int", nullable: false),
                    VIPZone = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Restaurants_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurants",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_RestaurantId",
                table: "Orders",
                column: "RestaurantId");
        }
    }
}
