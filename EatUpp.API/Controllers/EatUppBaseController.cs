﻿using System.Linq;
using System.Security.Claims;
using System.Collections.Generic;
using AutoMapper;
using EatUpp.BLL;
using EatUpp.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace EatUpp.API.Controllers
{
    [ApiController]
    public class EatUppBaseController : ControllerBase
    {
        #region Properties

        protected EatUppManager Manager { get; private set; }

        protected IMapper Mapper { get; private set; }

        protected IConfiguration Configuration { get; private set; }

        public Dictionary<int, string> ErrorDescriptions => Descriptions.GetInstance().ErrorDescriptions;

        public IDictionary<string, Claim> Claims => HttpContext.User.Claims.ToDictionary(claim => claim.Type);

        #endregion

        public EatUppBaseController(EatUppManager manager, IConfiguration configuration)
        {
            Manager = manager;
            Configuration = configuration;
        }
    }
}