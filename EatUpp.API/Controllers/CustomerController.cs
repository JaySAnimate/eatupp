﻿using System;
using EatUpp.BLL;
using EatUpp.Common.Enums;
using EatUpp.DTO.Models.Data;
using Microsoft.AspNetCore.Mvc;
using EatUpp.DTO.Models.Request;
using EatUpp.DTO.Models.Response;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace EatUpp.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Customer")]
    [ApiController]
    public class CustomerController : EatUppBaseController
    {
        public CustomerController(EatUppManager manager, IConfiguration configuration) : base(manager, configuration) { }

        [HttpGet]
        [Route("GetBookings")]
        public IEnumerable<BookingData> GetBookings() => Manager.GetBookingData(int.Parse(Claims[EClaimTypes.UserId.ToString()].Value));

        [HttpGet]
        [Route("GetRestaurants")]
        public IEnumerable<RestaurantData> GetRestaurants() => Manager.GetReastaurants();

        [HttpPost]
        [Route("PlaceOrder")]
        public PlaceOrderResponse PlaceOrder([FromBody] BookRequest order)
        {
            try
            {
                var orderData = Manager.RegisterOrder(order);
                if (orderData == null)
                    return new PlaceOrderResponse
                    {
                        Error = true,
                        ErrorResponse = new ErrorResponse
                        {
                            ErrorType = StatusCodes.OrderNotSaved,
                            ErrorDescription = ErrorDescriptions[(int)StatusCodes.OrderNotSaved]
                        }
                    };

                return new PlaceOrderResponse { Order = orderData };
            }
            catch (Exception ex)
            {
                return new PlaceOrderResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message
                    }
                };
            }
        }

        [HttpGet]
        [Route("GetMenuOf")]
        public IEnumerable<MenuItemData> GetMenu([FromQuery] int restaurantId) => Manager.GetMenuOf(restaurantId);
    }
}