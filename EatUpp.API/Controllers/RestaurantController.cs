﻿using System;
using EatUpp.DTO;
using EatUpp.BLL;
using System.Linq;
using EatUpp.DTO.Models;
using EatUpp.Common.Enums;
using EatUpp.DTO.Models.Data;
using Microsoft.AspNetCore.Mvc;
using EatUpp.DTO.Models.Request;
using EatUpp.DTO.Models.Response;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Internal;

namespace EatUpp.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(Roles = "Restaurant")]
    public class RestaurantController : EatUppBaseController
    {
        public RestaurantController(EatUppManager manager, IConfiguration configuration) : base(manager, configuration) { }

        [HttpPost]
        [Route("Approve")]
        public OrderStatus Approve(ApproveRequest approvement) => Manager.ApproveBook(approvement.OrderId) != 0 ? OrderStatus.Rejected : OrderStatus.Pending;

        [HttpPost]
        [Route("Reject")]
        public OrderStatus Reject(ApproveRequest approvement) => Manager.RejectBook(approvement.OrderId) != 0 ? OrderStatus.Rejected : OrderStatus.Pending;

        [HttpPost]
        [Route("InsertMenuEntities")]
        public InsertionResponse InsertMenuEntities(IEnumerable<MenuItemRequest> menuItems)
        {
            try
            {
                int initialCount = menuItems.Count();
                var insertedEntityCount = Manager.InsertMenuEntities(ref menuItems);
                if (insertedEntityCount != initialCount)
                    throw new DbUpdateException("Some Objects are not saved, See \"Absents\"");

                return new InsertionResponse { Status = (int)StatusCodes.Success };
            }
            catch (DbUpdateException ex)
            {
                return new InsertionResponse
                {
                    Error = true,
                    Absents = ex.Entries != null && ex.Entries.Any() ? ex.Entries : (IEnumerable<object>)menuItems,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.DBUpdateError,
                        ErrorDescription = ex.Message,
                    }
                };
            }
            catch (Exception ex)
            {
                return new InsertionResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message,
                    }
                };
            }
        }

        [HttpPost]
        [Route("GetOrders")]
        public GetDataResponse<IEnumerable<FoodOrderData>> GetOrders([FromBody] GetOrderRequest request)
        {
            try
            {
                return new GetDataResponse<IEnumerable<FoodOrderData>> { Data = Manager.GetFoodOrders(request.RestaurantId, request.From) };
            }
            catch (Exception ex)
            {
                return new GetDataResponse<IEnumerable<FoodOrderData>>
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message,
                    }
                };
            }
        }
    }
}