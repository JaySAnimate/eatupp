﻿using System;
using EatUpp.BLL;
using EatUpp.DTO.Models;
using EatUpp.Common.Enums;
using Microsoft.AspNetCore.Mvc;
using EatUpp.DTO.Models.Request;
using EatUpp.DTO.Models.Response;
using Microsoft.AspNetCore.Routing;
using Role = EatUpp.Common.Enums.Role;
using Microsoft.Extensions.Configuration;

namespace EatUpp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : EatUppBaseController
    {
        public UserController(EatUppManager manager, IConfiguration configuration) : base(manager, configuration) { }

        [HttpPost]
        [Route("SignUpRestaurant")]
        public SignUpResponse SignUpRestaurant([FromBody] RestaurantRequest restaurant)
        {
            try { return Manager.SignRestaurant(restaurant); }
            catch (Exception ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message
                    }
                };
            }
        }

        [HttpPost]
        [Route("SignUpCustomer")]
        public SignUpResponse SignUpCustomer([FromBody] CustomerRequest customer)
        {
            try { return Manager.SignCustomer(customer); }
            catch (Exception ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message
                    }
                };
            }
        }

        [HttpPost]
        [Route("LogIn")]
        public SignInResponse Login([FromBody] UserRequest loginModel)
        {
            try
            {
                if (Manager.TryLogIn(loginModel, out UserModel userModel))
                {
                    Manager.Validate(ref userModel, Configuration.GetValue<string>(ConfigurationKeys.JWTkey.ToString()));

                    return new SignInResponse
                    {
                        Id = userModel.RoleId == (int)Role.Customer ? userModel.Customer.Id : userModel.Restaurant.Id,
                        Name = userModel.Login,
                        Email = userModel.RoleId == (int)Role.Customer ? userModel.Customer.Email : userModel.Restaurant.Email,
                        RoleId = userModel.RoleId,
                        Token = userModel.Token
                    };
                }
                else
                {
                    return new SignInResponse
                    {
                        Error = true,
                        ErrorResponse = new ErrorResponse
                        {
                            ErrorType = StatusCodes.UserNotFound,
                            ErrorDescription = ErrorDescriptions[(int)StatusCodes.UserNotFound]
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                return new SignInResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message
                    }
                };
            }
        }

        [HttpGet]
        [Route("FastSign")]
        public SignUpResponse FastSign(string email)
        {
            try
            {
                var response = Manager.FastSign(email);
                response.Token = Manager.Validate(response.Email, Configuration[ConfigurationKeys.JWTkey.ToString()]);
                return response;
            }
            catch (Exception ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message
                    }
                };
            }
        }
    }
}