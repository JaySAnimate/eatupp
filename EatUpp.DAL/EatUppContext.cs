﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using EatUpp.DAL.EntityConfigurations;

namespace EatUpp.DAL
{
    public class EatUppContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DbSet<Event> Events { get; set; }
        
        public DbSet<FoodCategory> FoodCategories { get; set; }
        
        public DbSet<Email> Emails { get; set; }
        
        public DbSet<FoodOrder> FoodOrders { get; set; }
        
        public DbSet<MenuItem> MenuItems { get; set; }
        
        public DbSet<Book> Bookings { get; set; }
        
        public DbSet<Restaurant> Restaurants { get; set; }
        
        public DbSet<Role> Roles { get; set; }
        
        public DbSet<SignalRIdentifier> SignalRIdentifiers { get; set; }
        
        public DbSet<User> Users { get; set; }
        
        public DbSet<UserRole> UserRoles { get; set; }


        public EatUppContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new SignalRIdentifierConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new EmailConfiguration());
            modelBuilder.ApplyConfiguration(new RestaurantConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new FoodCategoryConfiguration());
            modelBuilder.ApplyConfiguration(new MenuItemConfiguration());
            modelBuilder.ApplyConfiguration(new FoodOrderConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new BookingConfiguration());
            modelBuilder.ApplyConfiguration(new EventConfiguration());
        }
    }
}