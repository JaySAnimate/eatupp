﻿namespace EatUpp.DAL.EntityRepositories
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}