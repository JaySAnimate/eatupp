﻿using System;
using EatUpp.DAL.Entities;
using System.Collections.Generic;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IFoodOrderRepository : IRepository<FoodOrder>
    {
        IEnumerable<FoodOrder> GetOrders(int restaurantId, DateTime? from);
    }
}