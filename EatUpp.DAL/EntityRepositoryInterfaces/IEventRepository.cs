﻿using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IEventRepository : IRepository<Event>
    {

    }
}