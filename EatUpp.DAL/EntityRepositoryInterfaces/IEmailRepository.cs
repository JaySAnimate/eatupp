﻿using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IEmailRepository : IRepository<Email>
    {

    }
}