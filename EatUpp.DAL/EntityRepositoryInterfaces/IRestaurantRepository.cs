﻿using EatUpp.DAL.Entities;
using System.Collections.Generic;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        IEnumerable<Restaurant> TopRatedRestaurants { get; }

        IEnumerable<Restaurant> WithVIPZone { get; }

        IEnumerable<Restaurant> WithNonSmokingZone { get; }

        IEnumerable<FoodCategory> GetCategories(int restaurantId);

        Restaurant GetByUserId(int userId);

        string GetSRID(int restaurantId);
    }
}