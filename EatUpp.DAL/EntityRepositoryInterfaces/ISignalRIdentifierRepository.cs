﻿using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface ISignalRIdentifierRepository : IRepository<SignalRIdentifier>
    {

    }
}