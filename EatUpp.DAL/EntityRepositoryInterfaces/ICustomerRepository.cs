﻿using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Customer GetByUserId(int userId);
    }
}