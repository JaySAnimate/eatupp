﻿using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IUserRepository : IRepository<User>
    {
        bool Exists(string login, out User user);

        User GetWithConnections(int id);
    }
}