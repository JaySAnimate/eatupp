﻿using EatUpp.DAL.Entities;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {
        
    }
}