﻿using EatUpp.DAL.Entities;
using System.Collections.Generic;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IOrderRepository : IRepository<Book>
    {
        IEnumerable<Book> GetForCustomer(int customerId);
    }
}