﻿using EatUpp.DAL.Entities;
using System.Collections.Generic;

namespace EatUpp.DAL.EntityRepositoryInterfaces
{
    public interface IMenuItemsRepository : IRepository<MenuItem>
    {
        IEnumerable<MenuItem> GetAllForResataurant(int restaurantId);
    }
}