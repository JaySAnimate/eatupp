﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        protected EatUppContext _context;

        public Repository(EatUppContext context) => _context = context;

        public void Add(TEntity entity) => _context.Set<TEntity>().Add(entity);
        
        public void AddRange(IEnumerable<TEntity> entities) => _context.Set<TEntity>().AddRange(entities);
        
        public virtual TEntity Get(int id) => _context.Set<TEntity>().Find(id);

        public IQueryable<TEntity> GetRange(int[] ids) => _context.Set<TEntity>().Where(entity => ids.Contains(entity.Id));

        public IQueryable<TEntity> GetAll() => _context.Set<TEntity>();

        public void Remove(TEntity entity) => _context.Set<TEntity>().Remove(entity);
    }
}