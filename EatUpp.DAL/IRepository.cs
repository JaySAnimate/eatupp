﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        TEntity Get(int id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetRange(int[] ids);
    }
}