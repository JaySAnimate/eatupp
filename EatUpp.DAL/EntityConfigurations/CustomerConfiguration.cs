﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Firstname).HasColumnType("NVARCHAR(25)");
            builder.Property(x => x.Lastname).HasColumnType("NVARCHAR(25)");
            builder.Property(x => x.Phone).HasColumnType("NVARCHAR(50)");
            
            builder.Property(x => x.EmailVerified).HasColumnType("BIT");


            builder.HasOne(x => x.User)
                   .WithOne(x => x.Customer)
                   .HasForeignKey<Customer>(x => x.UserId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.Email)
                  .WithOne(x => x.Customer)
                  .HasForeignKey<Customer>(x => x.EmailId)
                  .OnDelete(DeleteBehavior.Cascade);
        }
    }
}