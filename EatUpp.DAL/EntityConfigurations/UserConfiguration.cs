﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Login).HasColumnType("NVARCHAR(100)");
            builder.HasAlternateKey(x => x.Login);
            builder.Property(x => x.Password).HasColumnType("NVARCHAR(100)");

 
            builder.HasOne(x => x.SignalRIdentifier)
                  .WithMany(x => x.Users)
                  .HasForeignKey(x => x.SignalRIdentifierId)
                  .OnDelete(DeleteBehavior.Cascade);
        }
    }
}