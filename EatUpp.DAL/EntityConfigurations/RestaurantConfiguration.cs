﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class RestaurantConfiguration : IEntityTypeConfiguration<Restaurant>
    {
        public void Configure(EntityTypeBuilder<Restaurant> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.RestaurantName).HasColumnType("NVARCHAR(50)");
            builder.Property(x => x.RestaurantDescription).HasColumnType("NVARCHAR(100)");
            builder.Property(x => x.Logo).HasColumnType("IMAGE");
            builder.Property(x => x.Address).HasColumnType("NVARCHAR(50)");
            builder.Property(x => x.RestaurantStatus).HasColumnType("BIT");
            builder.Property(x => x.RVIPZone).HasColumnType("BIT");
            builder.Property(x => x.RNonSmokingZone).HasColumnType("BIT");
            builder.Property(x => x.RaitingSum).HasColumnType("INT");
            builder.Property(x => x.RaitingCount).HasColumnType("INT");
            builder.Property(x => x.Raiting).HasColumnType("DECIMAL(5,2)");
            builder.Property(x => x.Phone).HasColumnType("NVARCHAR(50)");
            builder.Property(x => x.Email).HasColumnType("NVARCHAR(50)");
            builder.HasAlternateKey(x => x.Email);
            builder.Property(x => x.Verified).HasColumnType("BIT");
            
           
            builder.HasOne(x => x.User)
                   .WithOne(x => x.Restaurant)
                   .HasForeignKey<Restaurant>(x => x.UserId)
                   .OnDelete(DeleteBehavior.NoAction);
        }
    }
}