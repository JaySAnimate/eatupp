﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class MenuItemConfiguration : IEntityTypeConfiguration<MenuItem>
    {
        public void Configure(EntityTypeBuilder<MenuItem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.FoodName).HasColumnType("NVARCHAR(45)");
            builder.Property(x => x.FoodPrice).HasColumnType("DECIMAL(5,2)");
            builder.Property(x => x.FoodDescription).HasColumnType("NVARCHAR(150)");
            builder.Property(x => x.FoodImage).HasColumnType("IMAGE");


            builder.HasOne(x => x.FoodCategory)
                   .WithMany(x => x.MenuItems)
                   .HasForeignKey(x => x.FoodCategoryId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.Restaurant)
                   .WithMany(x => x.MenuItems)
                   .HasForeignKey(x => x.RestaurantId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}