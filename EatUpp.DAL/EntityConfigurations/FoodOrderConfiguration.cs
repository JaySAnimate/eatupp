﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class FoodOrderConfiguration : IEntityTypeConfiguration<FoodOrder>
    {
        public void Configure(EntityTypeBuilder<FoodOrder> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.OrderTime).HasColumnType("DATETIME");
            builder.Property(x => x.OrderByMenu).HasColumnType("NVARCHAR(MAX)");
            builder.Property(x => x.TableId).HasColumnType("INT");

            
            builder.HasOne(x => x.Restaurant)
                   .WithMany(x => x.FoodOrders)
                   .HasForeignKey(x => x.RestaurantId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}