﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class SignalRIdentifierConfiguration : IEntityTypeConfiguration<SignalRIdentifier>
    {
        public void Configure(EntityTypeBuilder<SignalRIdentifier> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.SRID).HasColumnType("NVARCHAR(100)");
        }
    }
}