﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class EmailConfiguration : IEntityTypeConfiguration<Email>
    { 
        public void Configure(EntityTypeBuilder<Email> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.E_mail).HasColumnType("NVARCHAR(50)");
            builder.HasAlternateKey(x => x.E_mail);


            builder.HasOne(x => x.SignalRIdentifier)
                  .WithMany(x => x.Emails)
                  .HasForeignKey(x => x.SignalRIdentifierId)
                  .OnDelete(DeleteBehavior.Restrict);
        }
    }
}