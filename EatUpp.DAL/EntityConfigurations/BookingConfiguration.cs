﻿using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EatUpp.DAL.EntityConfigurations
{
    public class BookingConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.ChairCount).HasColumnType("INT");
            builder.Property(x => x.OrderDate).HasColumnType("DATETIME");
            builder.Property(x => x.CheckDate).HasColumnType("DATETIME");
            builder.Property(x => x.CancelDate).HasColumnType("DATETIME");
            builder.Property(x => x.VIPZone).HasColumnType("BIT");
            builder.Property(x => x.NonSmokingZone).HasColumnType("BIT");
            builder.Property(x => x.Comments).HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Approved).HasColumnType("INT");

            
            builder.HasOne(x => x.Restaurant)
                   .WithMany(x => x.Bookings)
                   .HasForeignKey(x => x.RestaurantId)
                   .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Customer)
                   .WithMany(x => x.Bookings)
                   .HasForeignKey(x => x.CustomerId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
