﻿using System.Linq;
using EatUpp.DAL.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public  class MenuItemsRepository : Repository<MenuItem>, IMenuItemsRepository
    {
        public MenuItemsRepository(EatUppContext context) : base(context) { }

        public IEnumerable<MenuItem> GetAllForResataurant(int restaurantId) =>
            _context.MenuItems.Where(item => item.RestaurantId == restaurantId).Include(menuItem => menuItem.FoodCategory).AsEnumerable();
    }
}