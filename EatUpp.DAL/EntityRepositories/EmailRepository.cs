﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class EmailRepository : Repository<Email>, IEmailRepository 
    {
        public EmailRepository(EatUppContext context) : base(context) { }
    }
}