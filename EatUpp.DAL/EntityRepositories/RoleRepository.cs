﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(EatUppContext context) : base(context) { }
    }
}