﻿using System.Linq;
using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(EatUppContext context) : base(context) { }

        public Customer GetByUserId(int userId) =>
            _context.Customers.Where(customerEntity => customerEntity.UserId == userId).FirstOrDefault();
    }
}