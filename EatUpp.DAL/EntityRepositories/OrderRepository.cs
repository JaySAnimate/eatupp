﻿using System.Linq;
using EatUpp.DAL.Entities;
using System.Collections.Generic;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class OrderRepository : Repository<Book>, IOrderRepository
    {
        public OrderRepository(EatUppContext context) : base(context) { }

        public IEnumerable<Book> GetForCustomer(int customerId) =>
            _context.Bookings.Where(order => order.CustomerId == customerId);
    }
}