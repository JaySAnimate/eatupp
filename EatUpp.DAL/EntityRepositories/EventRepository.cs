﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class EventRepository : Repository<Event>, IEventRepository
    {
        public EventRepository(EatUppContext context) : base(context) { }
    }
}