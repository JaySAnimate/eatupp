﻿using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class SignalRIdentifierRepository : Repository<SignalRIdentifier>, ISignalRIdentifierRepository
    {
        public SignalRIdentifierRepository(EatUppContext context) : base(context) { }
    }
}