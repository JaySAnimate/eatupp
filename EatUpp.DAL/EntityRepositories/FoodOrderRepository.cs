﻿using System;
using System.Linq;
using EatUpp.DAL.Entities;
using System.Collections.Generic;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class FoodOrderRepository : Repository<FoodOrder>, IFoodOrderRepository
    {
        public FoodOrderRepository(EatUppContext context) : base(context) { }

        public IEnumerable<FoodOrder> GetOrders(int restaurantId, DateTime? from)
        {
            if (from is null)
                return GetAll().Where(order => order.RestaurantId == restaurantId);

            return GetAll().Where(order => order.RestaurantId == restaurantId && order.OrderTime > from);
        }
    }
}