﻿using System.Linq;
using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class UserRoleRepository : Repository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(EatUppContext context) : base(context) { }

        public override UserRole Get(int userId) => _context.UserRoles.Where(userRoleEntity => userRoleEntity.UserId == userId).FirstOrDefault();
    }
}