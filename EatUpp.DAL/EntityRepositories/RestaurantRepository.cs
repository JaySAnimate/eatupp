﻿using System.Linq;
using EatUpp.DAL.Entities;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(EatUppContext context) : base(context) { }

        public IEnumerable<Restaurant> TopRatedRestaurants => _context.Restaurants.OrderByDescending(rest => rest.Raiting).Take(5);

        public IEnumerable<Restaurant> WithNonSmokingZone => _context.Restaurants.Where(rest => rest.RNonSmokingZone == true);

        public IEnumerable<Restaurant> WithVIPZone => _context.Restaurants.Where(rest => rest.RVIPZone == true);

        public IEnumerable<FoodCategory> GetCategories(int restaurantId) =>
            _context.MenuItems.Where(item => item.RestaurantId == restaurantId).Select(item => item.FoodCategory).Distinct();

        public Restaurant GetByUserId(int userId) =>
            _context.Restaurants.Where(restaurantEntity => restaurantEntity.UserId == userId).FirstOrDefault();

        public string GetSRID(int restaurantId) =>
            _context.Restaurants.Include(rest => rest.User.SignalRIdentifier).FirstOrDefault(rest => rest.Id == restaurantId).User.SignalRIdentifier.SRID;
    }
}