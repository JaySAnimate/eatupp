﻿using System.Linq;
using EatUpp.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.EntityRepositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(EatUppContext context) : base(context) { }

        public bool Exists(string login, out User user)
        {
            user = _context.Users.Include(user => user.Customer)
                                 .Include(user => user.Restaurant)
                                 .Include(user => user.UserRoles)
                                 .Include(user => user.SignalRIdentifier)
                                 .FirstOrDefault(user => user.Login == login);

            return user != null;
        } 
        
        public User GetWithConnections(int id) =>
            _context.Users.Include(user => user.Customer)
                          .Include(user => user.Restaurant)
                          .Include(user => user.UserRoles)
                          .Include(user => user.SignalRIdentifier)
                          .FirstOrDefault(user => user.Id == id);
    }
}