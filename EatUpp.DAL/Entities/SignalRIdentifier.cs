﻿using System.Collections.Generic;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
   public class SignalRIdentifier : IEntity
    {
        public int Id { get; set; }

        public string SRID { get; set; }


        public ICollection<Email> Emails { get; set; }

        public ICollection<User> Users { get; set; }
    }
}