﻿using System.Collections.Generic;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
    public class Customer : IEntity
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int EmailId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Phone { get; set; }

        public bool? EmailVerified { get; set; }


        public User User { get; set; }

        public Email Email { get; set; }

        public ICollection<Book> Bookings { get; set; }
    }
}