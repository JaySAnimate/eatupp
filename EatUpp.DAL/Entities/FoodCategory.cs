﻿using System.Collections.Generic;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
    public class FoodCategory : IEntity
    {
        public int Id { get; set; }

        public string Category { get; set; }


        public ICollection<MenuItem> MenuItems { get; set; }
    }
}