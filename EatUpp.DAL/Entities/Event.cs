﻿using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
    public class Event : IEntity
    {
        public int Id { get ; set ; }

        public int RestaurantId { get; set; }

        public string EventDescription { get; set; }


        public Restaurant Restaurant { get; set; }
    }
}