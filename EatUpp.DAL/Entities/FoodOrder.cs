﻿using System;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
    public class FoodOrder : IEntity
    {
        public int Id { get; set; }

        public int RestaurantId { get; set; }

        public DateTime? OrderTime { get; set; }

        public string OrderByMenu { get; set; }

        public int TableId { get; set; }


        public Restaurant Restaurant { get; set; }
    }
}