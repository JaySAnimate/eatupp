﻿using System.Collections.Generic;
using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }

        public int SignalRIdentifierId { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }


        public SignalRIdentifier SignalRIdentifier { get; set; }

        public Customer Customer { get; set; }
        
        public Restaurant Restaurant { get; set; }
        
        public ICollection<UserRole> UserRoles { get; set; }
    }
}