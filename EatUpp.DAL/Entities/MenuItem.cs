﻿using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
    public class MenuItem : IEntity
    {
        public int Id { get; set; }

        public int RestaurantId { get; set; }

        public int FoodCategoryId { get; set; }

        public string FoodName { get; set; }

        public decimal? FoodPrice { get; set; }

        public string FoodDescription { get; set; }

        public byte[] FoodImage { get; set; }


        public FoodCategory FoodCategory { get; set; }

        public Restaurant Restaurant { get; set; }
    }
}