﻿using EatUpp.DAL.EntityRepositories;

namespace EatUpp.DAL.Entities
{
    public class Email : IEntity
    {
        public int Id { get; set; }

        public int SignalRIdentifierId { get; set; }

        public string E_mail { get; set; }


        public Customer Customer { get; set; }

        public SignalRIdentifier SignalRIdentifier { get; set; }
    }
}