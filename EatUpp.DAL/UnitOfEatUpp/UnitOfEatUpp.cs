﻿using System.Linq;
using System.Threading.Tasks;
using EatUpp.DAL.EntityRepositories;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.UnitOfEatUpp
{
    public class UnitOfEatUpp : IUnitOfEatUpp
    {
        private readonly EatUppContext _context;

        public ICustomerRepository Customer { get; private set; }

        public IEventRepository Event { get; private set; }

        public IOrderRepository Order { get; private set; }

        public IRestaurantRepository Restaurant { get; private set; }

        public IRoleRepository Role { get; private set; }

        public IUserRepository User { get; private set; }

        public IUserRoleRepository UserRole { get; private set; }

        public ISignalRIdentifierRepository SignalRIdentifier { get; private set; }

        public IMenuItemsRepository MenuItems { get; private set; }

        public IFoodOrderRepository FoodOrders { get; private set; }

        public IEmailRepository Emails { get; private set; }

        public UnitOfEatUpp(
               EatUppContext context,
               ICustomerRepository customer,
               IEventRepository @event,
               IOrderRepository order,
               IRestaurantRepository restaurant,
               IRoleRepository role,
               IUserRepository user,
               IUserRoleRepository userRole,
               ISignalRIdentifierRepository signalRIdentifier,
               IMenuItemsRepository menuItem,
               IFoodOrderRepository foodOrder,
               IEmailRepository emails)
        {
            _context = context;
            Customer = customer;
            Event = @event;
            Order = order;
            Restaurant = restaurant;
            Role = role;
            User = user;
            UserRole = userRole;
            SignalRIdentifier = signalRIdentifier;
            MenuItems = menuItem;
            FoodOrders = foodOrder;
            Emails = emails;
        }

        public async Task<int> SaveAsync() => await _context.SaveChangesAsync();

        public int Save() => _context.SaveChanges();

        public void Dispose() => _context.Dispose();
    }
}