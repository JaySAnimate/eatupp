﻿using System;
using System.Threading.Tasks;
using EatUpp.DAL.EntityRepositories;
using EatUpp.DAL.EntityRepositoryInterfaces;

namespace EatUpp.DAL.UnitOfEatUpp
{
    public interface IUnitOfEatUpp : IDisposable
    {
        ICustomerRepository Customer { get; }
        IEventRepository Event { get; }
        IOrderRepository Order { get; }
        IRestaurantRepository Restaurant { get; }
        IRoleRepository Role { get; }
        IUserRepository User { get; }
        IUserRoleRepository UserRole { get; }
        ISignalRIdentifierRepository SignalRIdentifier { get; }
        IMenuItemsRepository MenuItems { get; }
        IFoodOrderRepository FoodOrders { get; }
        IEmailRepository Emails { get; }

        Task<int> SaveAsync();
        int Save();
    }
}