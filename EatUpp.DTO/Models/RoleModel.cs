﻿namespace EatUpp.DTO.Models
{
    public class RoleModel
    {
        public int Id { get ; set ; }

        public string TRole { get; set; }
    }
}