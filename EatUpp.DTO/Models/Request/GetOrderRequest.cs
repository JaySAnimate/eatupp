﻿using System;

namespace EatUpp.DTO.Models.Request
{
    public class GetOrderRequest
    {
        public int RestaurantId { get; set; }

        public DateTime? From { get; set; }
    }
}