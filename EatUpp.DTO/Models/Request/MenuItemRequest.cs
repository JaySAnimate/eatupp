﻿namespace EatUpp.DTO
{
    public class MenuItemRequest
    {
        public int RestaurantId { get; set; }

        public int FoodCategoryId { get; set; }

        public string FoodName { get; set; }

        public decimal? FoodPrice { get; set; }

        public string FoodDescription { get; set; }

        public byte[] FoodImage { get; set; }
    }
}