﻿namespace EatUpp.DTO.Models
{
    public class ApproveRequest
    {
        public int OrderId { get; set; }

        public bool IsApproved { get; set; }
    }
}