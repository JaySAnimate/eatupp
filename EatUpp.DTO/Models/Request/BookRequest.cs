﻿using System;

namespace EatUpp.DTO.Models.Request
{
    public class BookRequest
    {
        public int RestaurantId { get; set; }
        
        public int CustomerId { get; set; }

        public int ChairCount { get; set; }

        public DateTime OrderDate { get; set; }

        public bool VIPZone { get; set; }

        public bool NonSmokingZone { get; set; }
    }
}