﻿namespace EatUpp.DTO.Models.Response
{
    public struct SignInResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int RoleId { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }

        public bool Error { get; set; }

        public ErrorResponse ErrorResponse { get; set; }
    }
}