﻿using EatUpp.Common.Enums;

namespace EatUpp.DTO.Models.Response
{
    public struct ErrorResponse
    {
        public StatusCodes ErrorType { get; set; }

        public string ErrorDescription { get; set; }
    }
}