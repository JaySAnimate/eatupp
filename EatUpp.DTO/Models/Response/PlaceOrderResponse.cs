﻿using EatUpp.DTO.Models.Data;

namespace EatUpp.DTO.Models.Response
{
    public struct PlaceOrderResponse
    {
        public BookingData Order { get; set; }

        public bool Error { get; set; }

        public ErrorResponse ErrorResponse { get; set; }
    }
}