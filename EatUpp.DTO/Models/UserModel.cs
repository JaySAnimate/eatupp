﻿namespace EatUpp.DTO.Models
{
    public class UserModel
    {
        public int Id { get ; set ; }

        public string Login { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }

        public string Token { get; set; }

        public CustomerModel Customer { get; set; }

        public RestaurantModel Restaurant { get; set; }

        public SignalRIdentifierModel SignalRIdentifier { get; set; }
    }
}