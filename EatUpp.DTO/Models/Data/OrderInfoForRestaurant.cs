﻿using System;

namespace EatUpp.DTO.Models.Data
{
    public class OrderInfoForRestaurant
    {
        public int TableId { get; set; }

        public DateTime? OrderTime { get; set; }

        public string Order { get; set; }
    }
}