﻿using System;

namespace EatUpp.DTO.Models.Data
{
    public class FoodOrderData
    {
        public int RestaurantId { get; set; }

        public string Order { get; set; }

        public DateTime? OrderTime { get; set; }

        public int TableId { get; set; }
    }
}