﻿namespace EatUpp.DTO.Models.Data
{
    public class CustomerData
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
        
        public int UserId { get; set; }
    }
}