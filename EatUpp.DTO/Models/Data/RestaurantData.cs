﻿namespace EatUpp.DTO.Models.Data
{
    public class RestaurantData
    {
        public int Id { get; set; }
        
        public string RestaurantName { get; set; }

        public string RestaurantDescription { get; set; }

        public string Address { get; set; }

        public bool RVIPZone { get; set; }

        public bool RNonSmokingZone { get; set; }

        public byte[] Logo { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
    }
}