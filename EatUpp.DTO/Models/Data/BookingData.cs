﻿using System;

namespace EatUpp.DTO.Models.Data
{
    public class BookingData
    {
        public int Id { get; set; }

        public int RestaurantId { get; set; }

        public int CustomerId { get; set; }

        public int ChairCount { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? CheckDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public bool VIPZone { get; set; }

        public bool NonSmokingZone { get; set; }

        public int Approved { get; set; }

        public RestaurantData Restaurant { get; set; }

        public CustomerData Customer { get; set; }
    }
}