﻿namespace EatUpp.DTO.Models.Data
{
    public class MenuItemData
    {
        public int Id { get; set; }

        public int Count { get; set; }

        public int RestaurantId { get; set; }

        public int FoodCategoryId { get; set; }

        public string FoodName { get; set; }

        public decimal? FoodPrice { get; set; }

        public string FoodDescription { get; set; }

        public byte [] FoodImage { get; set; }

        public FoodCategoryData FoodCategory { get; set; }
    }
}