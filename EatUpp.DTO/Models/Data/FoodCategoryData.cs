﻿namespace EatUpp.DTO.Models.Data
{
    public class FoodCategoryData
    {
        public int Id { get; set; }

        public string Category { get; set; }
    }
}