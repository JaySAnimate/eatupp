﻿namespace EatUpp.DTO.Models.Data
{
    public class MessageModel
    {
        public string FromName { get; set; }

        public string Message { get; set; }
    }
}