﻿namespace EatUpp.DTO.Models
{
    public class EventModel
    {
        public int Id { get ; set ; }

        public int RestaurantId { get; set; }

        public string EventDescription { get; set; }
    }
}