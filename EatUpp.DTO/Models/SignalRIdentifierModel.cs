﻿namespace EatUpp.DTO.Models
{
    public class SignalRIdentifierModel
    {
        public int Id { get; set; }

        public string RestaurantLocation { get; set; }

        public string SRID { get; set; }
    }
}