﻿namespace EatUpp.DTO.Models
{
    public class PostCard
    {
        public string RestaurantId { get; set; }

        public int TableId { get; set; }
    }
}