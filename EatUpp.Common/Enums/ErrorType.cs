﻿namespace EatUpp.Common.Enums
{
    public enum StatusCodes
    {
        Success,
        Crash,
        UserNotFound,
        OrderNotSaved,
        OneOrManyNotSaved,
        DBUpdateError
    }
}