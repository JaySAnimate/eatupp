﻿namespace EatUpp.Common.Enums
{
    public enum ConfigurationKeys
    {
        EatUpConnectionString,
        JWTkey
    }
}