﻿namespace EatUpp.Common.Enums
{
    public enum OrderStatus
    {
        Pending,
        Approved,
        Rejected
    }
}