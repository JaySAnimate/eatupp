﻿namespace EatUpp.Common.Enums
{
    public enum Role
    {
        None,

        Restaurant = 1,
        Customer = 2
    }
}