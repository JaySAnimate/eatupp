﻿using EatUpp.Common.Enums;
using System.Collections.Generic;

namespace EatUpp.Common
{
    public class Descriptions
    {
        #region Private Fields

        public static Descriptions _instance;
        public static object locker = new object();

        #endregion

        public Dictionary<int, string> ErrorDescriptions { get; } = new Dictionary<int, string>
        {
            { (int)StatusCodes.UserNotFound, InvalidCredentials },
            { (int)StatusCodes.OrderNotSaved, OrderNotPlaced }
        };

        #region ErrorConstants

        const string InvalidCredentials = "Invalid Username or Password";
        const string OrderNotPlaced = "The Order is not Placed, Try Again";

        #endregion

        #region Singleton

        private Descriptions() { }

        public static Descriptions GetInstance()
        {
            if (_instance == null)
                lock (locker)
                    if (_instance == null)
                        return new Descriptions();

            return _instance;
        }

        #endregion
    }
}