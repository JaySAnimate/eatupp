﻿namespace EatUpp.Common.Constants
{
    public static class Role
    {
        public const string None = "None";
        public const string Customer = "Customer";
        public const string Restaurant = "Restaurant";
    }
}