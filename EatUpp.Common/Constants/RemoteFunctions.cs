﻿namespace EatUpp.Common.Constants
{
    public static class RemoteFunctions
    {
        public const string None = "None";
        public const string Connected = "Connected";
        public const string Disconnected = "Disconnected";
        public const string BookReceived = "BookReceived";
        public const string CustomerPoked = "CustomerPoked";
        public const string MessageReceived = "MessageReceived";
        public const string FoodOrderReceived = "FoodOrderReceived";
        public const string RejectionReceived = "RejectionReceived";
        public const string ApprovementReceived = "ApprovementReceived";
        public const string NotifyRestaurantOnlineStatus = "NotifyRestaurantOnlineStatus";
    }
}