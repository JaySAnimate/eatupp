﻿using EatUpp.DAL.Entities;
using EatUpp.DTO.Models.Data;

namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        public int SaveFoodOrder(FoodOrderData orderData)
        {
            _unitOfEatUpp.FoodOrders.Add(_mapper.Map<FoodOrder>(orderData));
            return _unitOfEatUpp.Save();
        }
    }
}