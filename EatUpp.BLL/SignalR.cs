﻿namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        public string GetRestaurantSRID(int restaurantId) => _unitOfEatUpp.Restaurant.GetSRID(restaurantId);
    }
}