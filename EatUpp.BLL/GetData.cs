﻿using System;
using EatUpp.DTO.Models.Data;
using System.Collections.Generic;

namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        public IEnumerable<RestaurantData> GetReastaurants() =>
            _mapper.Map<IEnumerable<RestaurantData>>(_unitOfEatUpp.Restaurant.GetAll());

        public IEnumerable<BookingData> GetBookingData(int customerId) =>
            _mapper.Map<IEnumerable<BookingData>>(_unitOfEatUpp.Order.GetForCustomer(customerId));

        public IEnumerable<MenuItemData> GetMenuOf(int restaurantId) =>
            _mapper.Map<IEnumerable<MenuItemData>>(_unitOfEatUpp.MenuItems.GetAllForResataurant(restaurantId));

        public IEnumerable<FoodCategoryData> GetCategories(int restaurantId) =>
            _mapper.Map<IEnumerable<FoodCategoryData>>(_unitOfEatUpp.Restaurant.GetCategories(restaurantId));

        public IEnumerable<FoodOrderData> GetFoodOrders(int restaurantId, DateTime? from) =>
            _mapper.Map<IEnumerable<FoodOrderData>>(_unitOfEatUpp.FoodOrders.GetOrders(restaurantId, from));
    }
}