﻿using EatUpp.Common.Enums;
using EatUpp.DAL.Entities;
using EatUpp.DTO.Models.Data;
using System.Threading.Tasks;
using EatUpp.DTO.Models.Request;

namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        public BookingData RegisterOrder(BookRequest order)
        {
            var orderEntity = _mapper.Map<Book>(order);

            _unitOfEatUpp.Order.Add(orderEntity);

            if (_unitOfEatUpp.Save() == 0)
                return null;

            return _mapper.Map<BookingData>(orderEntity);
        }

        public int ApproveBook(int id)
        {
            _unitOfEatUpp.Order.Get(id).Approved = (int)OrderStatus.Approved;
            return _unitOfEatUpp.Save();
        }

        public int RejectBook(int id)
        {
            _unitOfEatUpp.Order.Get(id).Approved = (int)OrderStatus.Rejected;
            return _unitOfEatUpp.Save();
        }
    }
}