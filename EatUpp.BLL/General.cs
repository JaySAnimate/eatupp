﻿using AutoMapper;
using EatUpp.DAL.UnitOfEatUpp;

namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        private readonly IUnitOfEatUpp _unitOfEatUpp;
        private readonly IMapper _mapper;

        public EatUppManager(IUnitOfEatUpp unitOfEatUpp, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfEatUpp = unitOfEatUpp;
        }
    }
}