﻿using System;
using System.Text;
using EatUpp.DTO.Models;
using System.Security.Claims;
using EatUpp.Common.Enums;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using EatUpp.Common.Constants;

namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        public void Validate(ref UserModel user, string JWTkey)
        {   
            IEnumerable<Claim> Claims = new List<Claim>()
            {
                new Claim(EClaimTypes.UserId.ToString(), user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Login),
                new Claim(ClaimTypes.Name, user.SignalRIdentifier.SRID),
                new Claim(ClaimTypes.Role, ((Role)user.RoleId).ToString()),
                new Claim(ClaimTypes.Email, user.RoleId == (int)Role.Customer ? user.Customer.Email : user.Restaurant.Email)
            };

            var jwtToken = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(Claims),
                Expires = DateTime.UtcNow.AddYears(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JWTkey)), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();            
            user.Token = tokenHandler.WriteToken(tokenHandler.CreateToken(jwtToken));
        }

        public string Validate(string email, string JWTkey)
        {
            IEnumerable<Claim> Claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, email),
                new Claim(ClaimTypes.Role, Role.Customer.ToString()),
                new Claim(ClaimTypes.Email, email)
            };

            var jwtToken = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(Claims),
                Expires = DateTime.UtcNow.AddYears(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JWTkey)), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            return tokenHandler.WriteToken(tokenHandler.CreateToken(jwtToken));
        }
    }
}