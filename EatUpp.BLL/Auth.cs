﻿using System;
using System.Linq;
using EatUpp.DTO.Models;
using EatUpp.Common.Enums;
using EatUpp.DAL.Entities;
using EatUpp.DTO.Models.Request;
using EatUpp.DTO.Models.Response;
using Role = EatUpp.Common.Enums.Role;

namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        public SignUpResponse SignRestaurant(RestaurantRequest restaurantModel)
        {
            try
            {
                restaurantModel.Password = SecurePasswordHasher.Hash(restaurantModel.Password);

                Restaurant restaurant = _mapper.Map<Restaurant>(restaurantModel);

                var signalRIdentifier = new SignalRIdentifier { SRID = string.Join('#', restaurant.Address.Replace(" ", string.Empty), restaurant.Email) };
                _unitOfEatUpp.SignalRIdentifier.Add(signalRIdentifier);

                restaurant.User.SignalRIdentifierId = signalRIdentifier.Id;
                _unitOfEatUpp.Restaurant.Add(restaurant);

                UserRole userRoleEntity = new UserRole
                {
                    UserId = restaurant.User.Id,
                    RoleId = (int)Role.Restaurant
                };
                _unitOfEatUpp.UserRole.Add(userRoleEntity);
                _unitOfEatUpp.Save();

                return new SignUpResponse()
                {
                    Email = restaurant.Email,
                    RoleId = userRoleEntity.RoleId
                };
            }
            catch (Exception ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse()
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message
                    }
                };
            }
        }

        public SignUpResponse SignCustomer(CustomerRequest customerRequest)
        {
            try
            {
                customerRequest.Password = SecurePasswordHasher.Hash(customerRequest.Password);

                var customer = _mapper.Map<Customer>(customerRequest);
                var signalRIdentifier = new SignalRIdentifier { SRID = customer.Email.E_mail };

                _unitOfEatUpp.SignalRIdentifier.Add(signalRIdentifier);
                _unitOfEatUpp.Customer.Add(customer);

                var userRoleEntity = new UserRole
                {
                    UserId = customer.User.Id,
                    RoleId = (int)Role.Customer
                };
                _unitOfEatUpp.UserRole.Add(userRoleEntity);
                _unitOfEatUpp.Save();

                return new SignUpResponse
                {
                    Email = customer.Email.E_mail,
                    RoleId = userRoleEntity.RoleId
                };
            }
            catch (Exception ex)
            {
                return new SignUpResponse
                {
                    Error = true,
                    ErrorResponse = new ErrorResponse
                    {
                        ErrorType = StatusCodes.Crash,
                        ErrorDescription = ex.Message
                    }
                };
            }
        }

        public bool TryLogIn(UserRequest user, out UserModel userModel)
        {
            try
            {
                userModel = null;

                bool Excists = _unitOfEatUpp.User.Exists(user.Username, out User dbUser) && SecurePasswordHasher.Verify(user.Password, dbUser.Password);

                if (Excists)
                {
                    userModel = _mapper.Map<UserModel>(dbUser);
                    userModel.RoleId = dbUser.UserRoles.FirstOrDefault(userRole => userRole.UserId == dbUser.Id).RoleId;
                }

                return Excists;
            }
            catch { throw; }
        }

        public SignUpResponse FastSign(string email)
        {
            try
            {
                if (_unitOfEatUpp.Emails.GetAll().Any(emailEntity => emailEntity.E_mail == email))
                    return new SignUpResponse
                    {
                        Email = email,
                        RoleId = (int)Role.Customer
                    };

                var signalRIdentifier = new SignalRIdentifier { SRID = email };
                _unitOfEatUpp.SignalRIdentifier.Add(signalRIdentifier);

                var e_mail = new Email
                {
                    E_mail = email,
                    SignalRIdentifier = signalRIdentifier
                };
                _unitOfEatUpp.Emails.Add(e_mail);

                var customer = new Customer
                {
                    UserId = null,
                    Email = e_mail,
                };
                _unitOfEatUpp.Customer.Add(customer);
                _unitOfEatUpp.Save();

                return new SignUpResponse
                {
                    Email = customer.Email.E_mail,
                    RoleId = (int)Role.Customer
                };
            }
            catch { throw; }
        }
    }
}