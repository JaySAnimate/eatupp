﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using EatUpp.DTO;
using EatUpp.DAL.Entities;
using EatUpp.DAL.EntityRepositories;
using EatUpp.DAL.EntityRepositoryInterfaces;
using System.Linq;

namespace EatUpp.BLL
{
    public partial class EatUppManager
    {
        public int InsertMenuEntities(ref IEnumerable<MenuItemRequest> menuItemsInput)
        {
            var entities = _mapper.Map<IEnumerable<MenuItem>>(menuItemsInput);
            _unitOfEatUpp.MenuItems.AddRange(entities);
            
            var savedCount = _unitOfEatUpp.Save();
            if (savedCount != menuItemsInput.Count())
                menuItemsInput = _mapper.Map<IEnumerable<MenuItemRequest>>(entities.Where(entity => entity.Id == 0));

            return savedCount;
        }
    }
}