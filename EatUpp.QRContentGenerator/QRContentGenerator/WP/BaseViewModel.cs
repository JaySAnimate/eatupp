﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace QRContentGenerator.WP
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void SetProperty<T>(ref T field, T value,[CallerMemberName]string propertyName = "")
        {
            if (!(field is null) && field.Equals(value))
                return;

            field = value;
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}