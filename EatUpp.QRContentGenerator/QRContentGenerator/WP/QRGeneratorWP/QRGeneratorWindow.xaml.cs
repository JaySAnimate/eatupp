﻿using System;
using System.Windows;
using System.Windows.Input;

namespace QRContentGenerator.WP.QRGeneratorWP
{
    public partial class QRGeneratorWindow : Window
    {
        public QRGeneratorWindow() => InitializeComponent();

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Window_Deactivated(object sender, EventArgs e) => (sender as Window).Topmost = true;
    }
}
