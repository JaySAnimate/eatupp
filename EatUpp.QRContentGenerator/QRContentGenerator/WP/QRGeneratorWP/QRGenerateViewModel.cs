﻿using System.Windows;
using Newtonsoft.Json;
using System.Windows.Input;
using System.Threading.Tasks;
using AsyncAwaitBestPractices.MVVM;

namespace QRContentGenerator.WP.QRGeneratorWP
{
    public class QRGenerateViewModel : BaseViewModel
    {
        #region Properties

        public string _restourantId;
        public string RestaurantId
        {
            get => _restourantId;
            set => SetProperty(ref _restourantId, value);
        }

        public string _tabletId;
        public string TableId
        {
            get => _tabletId;
            set => SetProperty(ref _tabletId, value);
        }

        public string _json;
        public string JsonString
        {
            get => _json;
            set => SetProperty(ref _json, value);
        }

        #endregion

        public ICommand GenerateCommand => new AsyncCommand(async () =>
        {
            JsonString = await Task.Run(() => JsonConvert.SerializeObject(new
            {
                TableId,
                RestaurantId
            }));

            Clipboard.SetText(JsonString);
        });
    }
}